package model;

import javax.persistence.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * The {@code WeatherForecast} class stores data of a single forecast
 * and its date and time information.
 *
 * @author Matwey Ivanov
 * @author Georgij Krajnyukov
 * @version 0.5.1
 */
@Entity
@Table(name = "WEATHER_FORECAST",
        uniqueConstraints = @UniqueConstraint(columnNames = {"FORECAST_DATE", "DATE_DIFFERENCE",
        "TIME_OF_DAY", "SERVICE"}))
public final class WeatherForecast {
    /**
     * temperature measured in degrees Celsius
     */
    @Column(name = "TEMPERATURE")
    private final float temperature;

    /**
     * humidity measured in percentages
     */
    @Column(name = "HUMIDITY")
    private final int humidity;

    /**
     * weather situation
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "WEATHER_SITUATION")
    private final WeatherSituation weatherSituation;

    /**
     * wind speed measured in meters per second
     */
    @Column(name = "WIND_SPEED")
    private final float windSpeed;

    /**
     * wind direction
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "WIND_DIRECTION")
    private final WindDirection windDirection;

    /**
     * time of the day for which forecast was made
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "TIME_OF_DAY")
    private final TimeOfDay timeOfDay;

    /**
     * date for which forecast was made
     */
    @Column(name = "FORECAST_DATE")
    private final LocalDate forecastDate;

    /**
     * number of days before the date for which forecast was made
     * from the forecast creation date
     */
    @Column(name = "DATE_DIFFERENCE")
    private final int dateDifference;

    /**
     * forecast source service
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "SERVICE")
    private final Service service;

    /**
     * unique id which is required for database
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Constructs {@code WeatherForecast} default state. It is needed by hibernate,
     * and shouldn't be used for other purposes.
     */
    protected WeatherForecast() {
        temperature = Float.NaN;
        humidity = -1;
        weatherSituation = null;
        windSpeed = Float.NaN;
        windDirection = null;
        timeOfDay = null;
        forecastDate = null;
        dateDifference = -1;
        service = null;
    }

    /**
     * Constructs a {@code WeatherForecast} created from the
     * fields set on the builder.
     *
     * @param builder {@code Builder} from which this instance will be created
     *
     * @throws IllegalArgumentException if any fields have invalid value
     */
    private WeatherForecast(Builder builder) {
        this.temperature = builder.temperature;
        this.humidity = builder.humidity;
        this.weatherSituation = builder.weatherSituation;
        this.windSpeed = builder.windSpeed;
        this.windDirection = builder.windDirection;
        this.timeOfDay = builder.timeOfDay;
        this.forecastDate = builder.forecastDate;
        this.dateDifference = builder.dateDifference;
        this.service = builder.service;

        if (humidity < 0 || humidity > 100)
            throw new IllegalArgumentException("humidity (%) is out of valid range [0; 100]");
        if (Float.compare(windSpeed, 0.0f) < 0)
            throw new IllegalArgumentException("wind speed must not be negative");
        if (dateDifference < 0)
            throw new IllegalArgumentException("date difference must not be negative");
    }

    /**
     * A builder of {@code WeatherForecast} instances.
     *
     * @author Georgij Krajnyukov
     * @version 0.1.0
     */
    public static class Builder {
        /**
         * value for {@link WeatherForecast#temperature}
         */
        private Float temperature = null;

        /**
         * value for {@link WeatherForecast#humidity}
         */
        private Integer humidity = null;

        /**
         * value for {@link WeatherForecast#weatherSituation}
         */
        private WeatherSituation weatherSituation = null;

        /**
         * value for {@link WeatherForecast#windSpeed}
         */
        private Float windSpeed = null;

        /**
         * value for {@link WeatherForecast#windDirection}
         */
        private WindDirection windDirection = null;

        /**
         * value for {@link WeatherForecast#timeOfDay}
         */
        private TimeOfDay timeOfDay = null;

        /**
         * value for {@link WeatherForecast#forecastDate}
         */
        private LocalDate forecastDate = null;

        /**
         * value for {@link WeatherForecast#dateDifference}
         */
        private Integer dateDifference = null;

        /**
         * value for {@link WeatherForecast#service}
         */
        private Service service = null;

        /**
         * Constructs empty builder.
         */
        public Builder() {}

        /**
         * Sets temperature.
         *
         * @param temperature temperature measured in degrees Celsius
         *
         * @return this builder
         */
        public Builder temperature(float temperature) {
            this.temperature = temperature;
            return this;
        }

        /**
         * Sets humidity.
         *
         * @param humidity humidity measured in percentages
         *
         * @return this builder
         */
        public Builder humidity(int humidity) {
            this.humidity = humidity;
            return this;
        }

        /**
         * Sets weather situation.
         *
         * @param weatherSituation weather situation
         *
         * @return this builder
         *
         * @throws NullPointerException if {@code weatherSituation} is null
         */
        public Builder weatherSituation(WeatherSituation weatherSituation) {
            Objects.requireNonNull(weatherSituation);
            this.weatherSituation = weatherSituation;
            return this;
        }

        /**
         * Sets wind speed.
         *
         * @param windSpeed wind speed measured in meters per second
         *
         * @return this builder
         */
        public Builder windSpeed(float windSpeed) {
            this.windSpeed = windSpeed;
            return this;
        }

        /**
         * Sets wind direction.
         *
         * @param windDirection wind direction
         *
         * @return this builder
         *
         * @throws NullPointerException if {@code windDirection} is null
         */
        public Builder windDirection(WindDirection windDirection) {
            Objects.requireNonNull(windDirection);
            this.windDirection = windDirection;
            return this;
        }

        /**
         * Sets time of day.
         *
         * @param timeOfDay time of the day for which forecast was made
         *
         * @return this builder
         *
         * @throws NullPointerException if {@code timeOfDay} is null
         */
        public Builder timeOfDay(TimeOfDay timeOfDay) {
            Objects.requireNonNull(windDirection);
            this.timeOfDay = timeOfDay;
            return this;
        }

        /**
         * Sets forecast date.
         *
         * @param forecastDate date for which forecast was made
         *
         * @return this builder
         *
         * @throws NullPointerException if {@code forecastDate} is null
         */
        public Builder forecastDate(LocalDate forecastDate) {
            Objects.requireNonNull(forecastDate);
            this.forecastDate = forecastDate;
            return this;
        }

        /**
         * Sets date difference.
         *
         * @param dateDifference number of days before the date for which forecast was made
         * from the forecast creation date
         *
         * @return this builder
         */
        public Builder dateDifference(int dateDifference) {
            this.dateDifference = dateDifference;
            return this;
        }

        /**
         * Sets source service
         *
         * @param service service that gave data
         *
         * @return this builder
         */
        public Builder service(Service service){
            this.service = service;
            return this;
        }

        /**
         * Returns an instance of {@code WeatherForecast} created from the
         * fields set on this builder.
         *
         * @return {@code WeatherForecast} created from the fields of this builder
         *
         * @throws IllegalStateException if any required fields are not set
         * @throws IllegalArgumentException if any fields have invalid value
         */
        public WeatherForecast build() {
            if (Objects.isNull(temperature))
                throw new IllegalStateException("temperature is not set");
            if (Objects.isNull(humidity))
                throw new IllegalStateException("humidity is not set");
            if (Objects.isNull(weatherSituation))
                throw new IllegalStateException("weather situation is not set");
            if (Objects.isNull(windSpeed))
                throw new IllegalStateException("wind speed is not set");
            if (Objects.isNull(windDirection))
                throw new IllegalStateException("wind direction is not set");
            if (Objects.isNull(timeOfDay))
                throw new IllegalStateException("time of day is not set");
            if (Objects.isNull(forecastDate))
                throw new IllegalStateException("forecast date is not set");
            if (Objects.isNull(dateDifference))
                throw new IllegalStateException("date difference is not set");
            if (Objects.isNull(service))
                throw new IllegalStateException("service is not set");

            return new WeatherForecast(this);
        }
    }

    /**
     * Returns difference between two dates in total number of days.
     *
     * @param startDate the start date
     * @param endDate the end date
     *
     * @return total number of days between two dates. May be negative.
     */
    public static int dateDifference(LocalDate startDate, LocalDate endDate) {
        int result = startDate.getDayOfYear() - endDate.getDayOfYear();
        int minYear = Math.min(startDate.getYear(), endDate.getYear());
        int maxYear = Math.max(startDate.getYear(), endDate.getYear());
        for (int year = minYear; year < maxYear; ++year) {
            result += LocalDate.ofYearDay(year, 1).lengthOfYear();
        }
        return result;
    }

    /**
     * Returns difference between forecasted date and parameter date in total number of days.
     * Calls {@link #dateDifference(LocalDate, LocalDate)} with this
     * instance {@link #forecastDate} and the {@code date}.
     *
     * @param date the end date
     *
     * @return total number of days between the forecasted date and parameter date.
     * May be negative.
     */
    public int dateDifference(LocalDate date) {
        return dateDifference(forecastDate, date);
    }

    /**
     * Returns difference between two forecasts in total number of days.
     * Calls {@link #dateDifference(LocalDate, LocalDate)} with this
     * instance {@link #forecastDate} and the parameter {@code forecastDate}.
     *
     * @param other WeatherForecast instance forecasted on the end date
     *
     * @return total number of days between two forecasts. May be negative.
     */
    public int dateDifference(WeatherForecast other) {
        return dateDifference(this.forecastDate, other.forecastDate);
    }

    /**
     * Gets temperature measured in degrees Celsius.
     *
     * @return temperature measured in degrees Celsius
     */
    public float getTemperature() {
        return temperature;
    }

    /**
     * Gets humidity measured in percentages.
     *
     * @return humidity measured in percentages
     */
    public int getHumidity() {
        return humidity;
    }

    /**
     * Gets weather situation.
     *
     * @return weather situation
     */
    public WeatherSituation getWeatherSituation() {
        return weatherSituation;
    }

    /**
     * Gets wind speed measured in meters per second.
     *
     * @return wind speed measured in meters per second
     */
    public float getWindSpeed() {
        return windSpeed;
    }

    /**
     * Gets wind direction.
     *
     * @return wind direction
     */
    public WindDirection getWindDirection() {
        return windDirection;
    }

    /**
     * Gets time of the day for which forecast was made.
     *
     * @return time of the day for which forecast was made
     */
    public TimeOfDay getTimeOfDay() {
        return timeOfDay;
    }

    /**
     * Gets date for which forecast was made.
     *
     * @return date for which forecast was made
     */
    public LocalDate getForecastDate() {
        return forecastDate;
    }

    /**
     * Gets the date difference of this forecast.
     *
     * @return number of the days before the date for which forecast was made
     * from the forecast creation date.
     */
    public int getDateDifference() {
        return dateDifference;
    }

    /**
     * Gets forecast source service.
     *
     * @return source service
     */
    public Service getService() { return service; }

    /**
     * Gets id of this forecast.
     *
     * @return forecast id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id for this forecast
     *
     * @param id new id for this forecast
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns a {@code String} representation of this {@code WeatherForecast}.
     * At least it contains forecasted date, time of the day and date difference.
     *
     * @return a {@code String} representation of this {@code WeatherForecast}
     */
    @Override
    public String toString() {
        return String.format("%s(%s, %s, %s)[%d]",
                getClass().getName(), forecastDate.format(DateTimeFormatter.ISO_DATE),
                timeOfDay.toString(), service, dateDifference);
    }
}
