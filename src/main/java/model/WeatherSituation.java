package model;

/**
 * The {@code WeatherSituation} enum represents weather situations.
 *
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public enum WeatherSituation {
    /**
     * rainy weather
     */
    RAIN,
    /**
     * snowy weather. Matches the hail.
     */
    SNOW,
    /**
     * weather without clouds. Matches the sunny weather.
     */
    CLEAR,
    /**
     * weather with clouds
     */
    CLOUDY,
    /**
     * clouds produce lightning and thunder
     */
    THUNDERSTORM,
    /**
     * foggy weather.
     */
    FOG
}
