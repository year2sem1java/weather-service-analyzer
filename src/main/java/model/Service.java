package model;

/**
 * The {@code Service} enum represents weather web-services.
 *
 * @author Matwey Ivanov
 * @author Georgij Krajnyukov
 * @version 0.3.0
 */
public enum Service {
    /**
     * Yandex.Weather service
     */
    YANDEX_WEATHER,

    /**
     * Gismeteo service
     */
    GISMETEO,

    /**
     * AccuWeather service
     */
    ACCUWEATHER,

    /**
    * Foreca service
    */
    FORECA,

    /**
    * weather archive
    */
    ACTUAL
}
