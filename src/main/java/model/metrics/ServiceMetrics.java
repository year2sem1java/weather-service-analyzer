package model.metrics;

import javafx.util.Pair;
import model.WeatherForecast;

import java.util.List;

/**
 * The {@code ServiceMetrics} encapsulates difference estimation logic for
 * list and single object of {@code WeatherForecast} class.
 *
 * @author Matwey Ivanov
 * @author Georgij Krajnyukov
 * @version 0.2.0
 */
public interface ServiceMetrics {
    /**
     * Estimates difference between forecasted weather and actual weather.
     *
     * @param forecastedWeather weather forecasted on some date
     * @param actualWeather actual weather on this date
     *
     * @return estimation of forecast's difference
     */
    float estimate(WeatherForecast forecastedWeather, WeatherForecast actualWeather);

    /**
     * Estimates score of the service by forecast data.
     *
     * @param data list of paired by dates forecasts where key is a forecast
     * and value is an actual datum
     *
     * @return estimated service score
     */
    float estimate(List<Pair<WeatherForecast, WeatherForecast>> data);
}
