package model.metrics;

import javafx.util.Pair;
import model.WeatherForecast;
import model.WindDirection;

import java.util.List;

/**
 * The {@code GeneralServiceMetrics} estimates difference of list or single objects of
 * {@code WeatherForecast} class.
 * @author Matwey Ivanov
 * @author Georgij Krajnyukov
 * @version 0.4.4
 */
public class GeneralServiceMetrics implements ServiceMetrics {
    private static final double HUMIDEX_MIN_TEMPERATURE = 20;
    private static final double HUMIDEX_MIN_HUMIDITY = 40;

    private static final double WINDCHILL_MAX_TEMPERATURE = 10;
    //Convert km/h to m/s
    private static final double WINDCHILL_MIN_WIND_SPEED = 5.0 * 1000.0/3600.0;

    private static final double THERMAL_COMFORT_MAX_SCORE = 3.0;
    private static final double WEATHER_SITUATION_MAX_SCORE = 1.5;

    /**
     * This enumeration represents humidex degrees of comfort.
     *
     * @author Georgij Krajnyukov
     * @version 0.1.0
     */
    private enum HumidexComfortDegree {
        /**
         * a comfort level. This level has an infinite left endpoint,
         * so following constraints are applied: temperature >= 20 && humidity >= 40.
         * Therefore, comfort level corresponds to [20; 30)°C humidex
         * temperature interval. Interval length equals to 10.
         */
        COMFORT(10),
        /**
         * a slight discomfort level. Corresponds to [30; 40)°C humidex
         * temperature interval. Interval length equals to 10.
         */
        DISCOMFORT(10),
        /**
         * a slight discomfort level. Corresponds to [40; 46)°C humidex
         * temperature interval. Interval length equals to 6.
         */
        GREAT_DISCOMFORT(6),
        /**
         * a dangerous level. This level has an infinite right endpoint,
         * so it is considered to use current humidex record as a right bound
         * which is 53°C [1]. Therefore, this level corresponds to [46; 53)°C humidex
         * temperature interval. Interval length equals to 7.
         *
         * [1] <a href="https://ec.gc.ca/meteo-weather/default.asp?lang=En&n=5EBCEA59-1">
         * Canada's Top Ten Weather Stories for 2007</a>
         */
        DANGEROUS(7);

        /**
        * this level humidex temperature interval length
        */
        private final double humidexIntervalLength;

        /**
         * Constructs a new instance with parameter humidex interval length.
         *
         * @param humidexIntervalLength this level humidex interval length
         */
        HumidexComfortDegree(double humidexIntervalLength) {
            this.humidexIntervalLength = humidexIntervalLength;
        }

        /**
         * Gets this level humidex temperature interval length.
         *
         * @return this level humidex temperature interval length
         */
        public double getHumidexIntervalLength() {
            return humidexIntervalLength;
        }
    }

    /**
     * This enumeration represents wind chill hazards.
     *
     * @author Georgij Krajnyukov
     * @version 0.1.0
     */
    private enum WindChillHazard {
        /**
         * a low exposure risk hazard. This level has an infinite right endpoint,
         * but it is said that wind chill formula is applicable when temperature
         * <= 10°C && wind speed >= 5 km/h. Therefore, this hazard corresponds to
         * (-10; 0]°C wind chill temperature interval. Interval length equals to 10.
         */
        LOW_RISK(10),
        /**
         * a moderate exposure risk hazard. Corresponds to (-28;-10]°C wind chill
         * temperature interval. Interval length equals to 18.
         */
        MODERATE_RISK(18),
        /**
         * a high exposure risk hazard. Corresponds to (-40;-28]°C wind chill
         * temperature interval. Interval length equals to 12.
         */
        HIGH_RISK(12),
        /**
         * a very high exposure risk hazard. Corresponds to (-48;-40]°C wind chill
         * temperature interval. Interval length equals to 8.
         */
        VERY_HIGH_RISK(8),
        /**
         * a severe exposure risk hazard. Corresponds to (-55;-48]°C wind chill
         * temperature interval. Interval length equals to 7.
         */
        SEVERE_RISK(7),
        /**
         * an extreme exposure risk hazard. This level has an infinite left endpoint,
         * so it is considered to use current wind chill record as a left bound
         * which is -78°C [1]. Therefore, this hazard corresponds to [-78; -55]°C wind
         * chill temperature interval. Interval length equals to 24.
         *
         * [1] <a href="//https://www.canada.ca/en/environment-climate-change/services/weather-health/wind-chill-cold-weather/wind-chill-index.html#X-201501151120261">
         * Wind chill index</a>
         */
        EXTREME_RISK(24);

        /**
        * this hazard wind chill temperature interval length
        */
        private final double windChillIntervalLength;

        /**
         * Constructs a new instance with parameter wind chill interval length.
         *
         * @param windChillIntervalLength this level humidex interval length
         */
        WindChillHazard(double windChillIntervalLength) {
            this.windChillIntervalLength = windChillIntervalLength;
        }

        /**
         * Gets this level wind chill temperature interval length.
         *
         * @return this level wind chill temperature interval length
         */
        public double getWindChillIntervalLength() {
            return windChillIntervalLength;
        }
    }

    /**
     * Returns humidex degree of comfort value corresponding to parameter humidex
     * temperature.
     *
     * @param humidexTemperature humidex temperature measured in degrees Celsius
     *
     * @return humidex degree of comfort value corresponding to the parameter
     */
    private HumidexComfortDegree humidexComfortDegree(double humidexTemperature) {
        if (humidexTemperature < 30)
            return HumidexComfortDegree.COMFORT;
        if (humidexTemperature < 40)
            return HumidexComfortDegree.DISCOMFORT;
        if (humidexTemperature < 46)
            return HumidexComfortDegree.GREAT_DISCOMFORT;

        return HumidexComfortDegree.DANGEROUS;
    }

    /**
     * Returns wind chill hazard value corresponding to parameter wind chill temperature.
     *
     * @param windChillTemperature wind chill temperature measured in degrees Celsius
     *
     * @return wind chill hazard value corresponding to the parameter
     */
    private WindChillHazard windChillHazard(double windChillTemperature) {
        if (windChillTemperature > -10)
            return WindChillHazard.LOW_RISK;
        if (windChillTemperature > -28)
            return WindChillHazard.MODERATE_RISK;
        if (windChillTemperature > -40)
            return WindChillHazard.HIGH_RISK;
        if (windChillTemperature > -48)
            return WindChillHazard.VERY_HIGH_RISK;
        if (windChillTemperature > -55)
            return WindChillHazard.SEVERE_RISK;

        return WindChillHazard.EXTREME_RISK;
    }

    /**
     * Returns humidex temperature calculated from parameters.
     *
     * @param temperature air temperature measured in degrees Celsius
     * @param humidity relative humidity measured in percentages
     *
     * @return humidex temperature measured in degrees Celsius
     */
    private double humidex(double temperature, int humidity) {
        double dewPointTemperature = (temperature + 273) - (100.0 - humidity) / 5.0;
        double vaporPressure = 6.11 * Math.exp(5417.753*(1/273.16 - 1/(273.15 + dewPointTemperature)));
        return temperature + 5.0/9.0*(vaporPressure - 10);
    }

    /**
     * Returns wind chill temperature calculated from parameters.
     *
     * @param temperature air temperature measured in degrees Celsius
     * @param windSpeed wind speed measured in meters per second
     *
     * @return wind chill temperature measured in degrees Celsius
     */
    private double windChill(double temperature, double windSpeed) {
        //Convert to km/h
        temperature = temperature * 1000.0/3600.0;
        double poweredWindSpeed = Math.pow(windSpeed, 0.16);
        return 13.12 + 0.6215*temperature - 11.37*poweredWindSpeed
            + 0.3965*temperature*poweredWindSpeed;
    }

    private double temperatureScore(double forecastedTemperature, double actualTemperature) {
        final double COEFFICIENT = 0.47;
        double temperatureAbsError = Math.abs(actualTemperature - forecastedTemperature);
        return 1 / Math.pow(1.0 + COEFFICIENT*temperatureAbsError,
            COEFFICIENT*temperatureAbsError*temperatureAbsError);
    }

    private double humidityScore(int forecastedHumidity, int actualHumidity) {
        final double COEFFICIENT = 0.062;
        double humidityAbsError = Math.abs(actualHumidity - forecastedHumidity);
        return 1 / Math.pow(1 + COEFFICIENT*humidityAbsError,
            COEFFICIENT*humidityAbsError*humidityAbsError);
    }

    /**
     * Calculates wind speed score.
     *
     * @param actualWindSpeed wind speed that was that day
     * @param forecastedSpeed wind speed that was predicted for that day
     *
     * @return wind speed score [0, 1]
     */
    private double windSpeedScore(double actualWindSpeed, double forecastedSpeed) {
        if(actualWindSpeed < 0 || forecastedSpeed < 0)
            throw new IllegalArgumentException("WindSpeed can't be negative value");

        final double COEFFICIENT = 0.7;
        double windSpeedAbsError = Math.abs(actualWindSpeed - forecastedSpeed);
        return 1 / Math.pow(1 + COEFFICIENT*windSpeedAbsError,
            COEFFICIENT*windSpeedAbsError*windSpeedAbsError);
    }

    /**
     * Calculates forecast wind direction accuracy. It uses cosine similarity metric.
     *
     * @param actualDirection real wind direction that was that day
     * @param forecastedDirection predicted wind direction for that day
     *
     * @return accuracy of wind direction prediction
     */
    private double windDirectionScore(WindDirection actualDirection,
                                     WindDirection forecastedDirection) {
        if (actualDirection == WindDirection.NONE && forecastedDirection == WindDirection.NONE)
            return 1.0;

        if (actualDirection == WindDirection.NONE || forecastedDirection == WindDirection.NONE)
            return 0.0;

        return (1 + Math.cos(WindDirection.azimuthOf(actualDirection)
            - WindDirection.azimuthOf(forecastedDirection))) / 2;
    }

    /**
     * Estimates score of the service by forecast data. This method
     * gets forecasts with date difference != 0 and compares with data
     * received from this service with date difference == 0
     *
     * @param data list of paired by dates forecasts where key is a forecast
     * and value is an actual datum
     * @throws IllegalArgumentException if sizes of collections are not the same or collections are empty
     * @throws NullPointerException if params of method contains null
     *
     * @return service score
     */
    public float estimate(List<Pair<WeatherForecast, WeatherForecast>> data) {
        float sumEstimation = 0.0f;
        int estimationsCount = 0;
        for (var pair : data) {
            sumEstimation += estimate(pair.getKey(), pair.getValue());
            estimationsCount += 1;
        }
        return sumEstimation / estimationsCount;
    }

    /**
     * Estimates difference between forecasted weather and actual weather.
     *
     * @param forecastedWeather weather forecasted on some date
     * @param actualWeather actual weather on this date
     * @throws NullPointerException if any param is null
     *
     * @return estimation of difference in [0, 5] interval
     */
    public float estimate(WeatherForecast forecastedWeather, WeatherForecast actualWeather) {
        float actualTemperature = actualWeather.getTemperature();
        int actualHumidity = actualWeather.getHumidity();
        float actualWindSpeed = actualWeather.getWindSpeed();

        float forecastedTemperature = forecastedWeather.getTemperature();
        int forecastedHumidity = forecastedWeather.getHumidity();
        float forecastedWindSpeed = forecastedWeather.getWindSpeed();

        double result = 5.0f;

        //Wind chill constraints
        if (actualTemperature <= WINDCHILL_MAX_TEMPERATURE
            && forecastedTemperature <= WINDCHILL_MAX_TEMPERATURE
            && actualWindSpeed >= WINDCHILL_MIN_WIND_SPEED
            && forecastedWindSpeed >= WINDCHILL_MIN_WIND_SPEED) {
            double actualWindChill = windChill(actualTemperature, actualWindSpeed);
            double forecastedWindChill = windChill(forecastedTemperature, forecastedWindSpeed);

            WindChillHazard actualHazard = windChillHazard(actualWindChill);
            WindChillHazard forecastedHazard = windChillHazard(forecastedWindChill);

            if (forecastedHazard != actualHazard) {
                result -= THERMAL_COMFORT_MAX_SCORE;
            } else {
                result -= Math.abs(actualWindChill - forecastedWindChill)
                    / actualHazard.getWindChillIntervalLength();
            }
        }
        //Humidex restraints
        else if (actualTemperature > HUMIDEX_MIN_TEMPERATURE
                && forecastedTemperature > HUMIDEX_MIN_TEMPERATURE
                && actualHumidity > HUMIDEX_MIN_HUMIDITY
                && forecastedHumidity > HUMIDEX_MIN_HUMIDITY) {
            double actualHumidex = humidex(actualTemperature, actualHumidity);
            double forecastedHumidex = humidex(forecastedTemperature, forecastedHumidity);

            HumidexComfortDegree actualComfortDegree = humidexComfortDegree(actualHumidex);
            HumidexComfortDegree forecastedComfortDegree = humidexComfortDegree(forecastedHumidex);

            if (forecastedComfortDegree != actualComfortDegree) {
                result -= THERMAL_COMFORT_MAX_SCORE;
            } else {
                result -= Math.abs(actualHumidex - forecastedHumidex)
                    / forecastedComfortDegree.getHumidexIntervalLength();
            }
        } else {
            result -= 1 - temperatureScore(forecastedTemperature, actualTemperature);
            result -= 1 - humidityScore(forecastedHumidity, actualHumidity);
            result -= 1 - windSpeedScore(forecastedWindSpeed, actualWindSpeed);
        }

        if (forecastedWeather.getWeatherSituation() != actualWeather.getWeatherSituation())
            result -= WEATHER_SITUATION_MAX_SCORE;

        result -= (1 - windDirectionScore(actualWeather.getWindDirection(),
            forecastedWeather.getWindDirection())) / 2;

        return result > 0 ? (float)result : 0.0f;
    }
}
