package model.metrics;

import javafx.util.Pair;
import model.WeatherForecast;

import java.util.List;

/**
* {@code ServiceMetrics} that estimates temperature.
*
* @author Georgij Krajnyukov
* @version 0.2.1
*/
public class TemperatureServiceMetrics implements ServiceMetrics {
    /**
    * Constructs new instance.
    */
    public TemperatureServiceMetrics() {}

    /**
    * Returns forecasted temperature. Does not use actual data.
    *
    * @param forecastedWeather {@code WeatherForecast} containing forecasted temperature
    * @param actualWeather {@code WeatherForecast} containing actual temperature (not used)
    *
    * @return forecasted temperature value
    *
    * @see WeatherForecast
    */
    @Override
    public float estimate(WeatherForecast forecastedWeather, WeatherForecast actualWeather) {
        return forecastedWeather.getTemperature();
    }

    /**
    * Returns total absolute difference between forecasted and actual temperatures.
    *
    * @param data list of paired by dates forecasts where key is a forecast
     * and value is an actual datum
    *
    * @return total absolute difference between forecasted and actual temperatures
    */
    @Override
    public float estimate(List<Pair<WeatherForecast, WeatherForecast>> data) {
        float result = 0.0f;
        for (var pair : data) {
            result -= Math.abs(pair.getKey().getTemperature()
                        - pair.getValue().getTemperature());
        }
        return result;
    }
}
