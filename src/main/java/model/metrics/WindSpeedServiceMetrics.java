package model.metrics;

import javafx.util.Pair;
import model.WeatherForecast;

import java.util.List;

/**
* {@code ServiceMetrics} that estimates wind speed.
*
* @author Georgij Krajnyukov
* @version 0.2.1
*/
public class WindSpeedServiceMetrics implements ServiceMetrics {
    /**
    * Constructs new instance.
    */
    public WindSpeedServiceMetrics() {}

    /**
    * Returns forecasted wind speed. Does not use actual data.
    *
    * @param forecastedWeather {@code WeatherForecast} containing forecasted wind speed
    * @param actualWeather {@code WeatherForecast} containing actual wind speed (not used)
    *
    * @return forecasted wind speed value
    *
    * @see WeatherForecast
    */
    @Override
    public float estimate(WeatherForecast forecastedWeather, WeatherForecast actualWeather) {
        return forecastedWeather.getWindSpeed();
    }

    /**
    * Returns total absolute difference between forecasted and actual wind speed.
    *
    * @param data list of paired by dates forecasts where key is a forecast
     * and value is an actual datum
    *
    * @return total absolute difference between forecasted and actual wind speed
    *
    * @throws IllegalArgumentException if dimensions of parameters does not match
    */
    @Override
    public float estimate(List<Pair<WeatherForecast, WeatherForecast>> data) {
        float result = 0.0f;
        for (var pair : data) {
            result -= Math.abs(pair.getKey().getWindSpeed()
                        - pair.getValue().getWindSpeed());
        }
        return result;
    }
}
