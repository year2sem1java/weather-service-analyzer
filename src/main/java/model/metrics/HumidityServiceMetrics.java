package model.metrics;

import javafx.util.Pair;
import model.WeatherForecast;

import java.util.List;

/**
* {@code ServiceMetrics} that estimates humidity.
*
* @author Georgij Krajnyukov
* @version 0.2.1
*/
public class HumidityServiceMetrics implements ServiceMetrics {
    /**
    * Constructs new instance.
    */
    public HumidityServiceMetrics() {}

    /**
    * Returns forecasted humidity. Does not use actual data.
    *
    * @param forecastedWeather {@code WeatherForecast} containing forecasted humidity
    * @param actualWeather {@code WeatherForecast} containing actual humidity (not used)
    *
    * @return forecasted humidity value
    *
    * @see WeatherForecast
    */
    @Override
    public float estimate(WeatherForecast forecastedWeather, WeatherForecast actualWeather) {
        return forecastedWeather.getHumidity();
    }

    /**
    * Returns total absolute difference between forecasted and actual humidity.
    *
    * @param data list of paired by dates forecasts where key is a forecast
     * and value is an actual datum
    *
    * @return total absolute difference between forecasted and actual humidity
    *
    * @throws IllegalArgumentException if dimensions of parameters does not match
    */
    @Override
    public float estimate(List<Pair<WeatherForecast, WeatherForecast>> data) {
        float result = 0.0f;
        for (var pair : data) {
            result -= Math.abs(pair.getKey().getHumidity()
                        - pair.getValue().getHumidity());
        }
        return result;
    }
}
