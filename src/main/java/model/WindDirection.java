package model;


/**
 * The {@code WindDirection} enum represents 8-wind compass rose
 * directions.
 *
 * @author Georgij Krajnyukov
 * @version 0.2.0
 */
public enum WindDirection {
    /**
     * towards the north (N)
     */
    NORTH,
    /**
     * towards the west (W)
     */
    WEST,
    /**
     * towards the south (S)
     */
    SOUTH,
    /**
     * towards the east (E)
     */
    EAST,
    /**
     * towards the northwest (NW)
     */
    NORTHWEST,
    /**
     * towards the northeast (NE)
     */
    NORTHEAST,
    /**
     * towards the southwest (SW)
     */
    SOUTHWEST,
    /**
     * towards the southeast (SE)
     */
    SOUTHEAST,
    /**
     * towards nowhere (there is no wind)
     */
    NONE;

    public static float azimuthOf(WindDirection direction) {
        return switch (direction) {
            case NORTH -> 0;
            case NORTHEAST -> (float) Math.PI / 4;
            case EAST -> (float) Math.PI / 2;
            case SOUTHEAST -> (float) Math.PI * 3 / 4;
            case SOUTH -> (float) Math.PI;
            case SOUTHWEST -> (float) Math.PI * 5 / 4;
            case WEST -> (float) Math.PI * 3 / 2;
            case NORTHWEST -> (float) Math.PI * 7 / 4;
            case NONE -> Float.NaN;
        };
    }
}
