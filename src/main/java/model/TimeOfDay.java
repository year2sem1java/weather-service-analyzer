package model;

/**
 * The {@code TimeOfDay} enum represents times of the day.
 *
 * @author Georgij Krajnyukov
 * @version 0.2.0
 */
public enum TimeOfDay {
    /**
     * the time from 6 a.m. to 12 a.m., exclusive.
     */
    MORNING,
    /**
     * the time from 12 a.m. to 6 p.m., exclusive
     */
    AFTERNOON,
    /**
     * the time from 6 p.m. to 12 p.m., exclusive
     */
    EVENING,
    /**
     * the time from 12 p.m. to 6 a.m., exclusive
     */
    MIDNIGHT,
}
