package view.controllers;

import javafx.event.Event;
import javafx.event.EventType;

import java.util.Locale;

public class LanguageChangedEvent extends Event {
    public static final EventType<LanguageChangedEvent> CHANGED =
            new EventType<>(ANY, "CHANGED");

    private final Locale locale;

    public LanguageChangedEvent(EventType<? extends Event> eventType, Locale locale) {
        super(eventType);
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }
}
