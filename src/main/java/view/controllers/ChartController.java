package view.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Service;

import java.net.URL;
import java.util.*;

/**
* The {@code ChartController} manages chart panel components.
*
* @author Georgij Krajnyukov
* @author Maxim Sergeev
* @version 0.2.1
*/
public class ChartController implements Initializable {
    /**
    * chart component
    */
    @FXML
    BarChart<String, Float> chart;

    @FXML
    NumberAxis yAxis;

    /**
    * chart info component
    */
    @FXML
    private VBox chartInfo;

    private ResourceBundle resourceBundle;

    /**
     * map, which contains a service and series that matches it
     */
    Map<Service, XYChart.Series<String, Float>> seriesMap;

    /**
    * Constructs new instance.
    */
    public ChartController() {}

    /**
    * Initializes this instance. Called after all components are created.
    * Creates one series for each service from {@code Service.values()}.
    * For each service color does not change and has one of the default values
    * (depends on the order).
    */
    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;

        seriesMap = new EnumMap<>(Service.class);
        var chartData = chart.getData();
        for (Service service : Service.values()) {
            XYChart.Series<String, Float> series = new XYChart.Series<>();
            series.setName(resourceBundle.getString("chart.legend" + "." + service));
            seriesMap.put(service, series);
            chartData.add(series);
        }

        chartInfo.setBorder(new Border(new BorderStroke(Color.DIMGRAY, BorderStrokeStyle.SOLID, new CornerRadii(10),
                BorderStroke.THIN)));
    }

    /**
    * Sets services top in chart info.
    *
    * @param services services top to be set
    */
    void setChartInfoServicesTop(List<Service> services) {
        Font font = new Font("Colibri", 14);
        chartInfo.getChildren().clear();

        for (int i = 0; i < services.size(); ++i) {
            Service service = services.get(i);
            String serviceName = resourceBundle.getString("chartInfo." + service.toString());

            Label newLabel = new Label();
            newLabel.setText(String.format("%d. %s\n", i + 1, serviceName));
            newLabel.setFont(font);
            newLabel.setBorder(new Border(new BorderStroke(Color.DIMGRAY, BorderStrokeStyle.SOLID, new CornerRadii(5),
                    BorderStroke.THIN)));
            newLabel.setPadding(new Insets(1, 10, 1, 10));
            newLabel.setPrefWidth(160);
            chartInfo.getChildren().add(newLabel);
        }
    }
}
