package view.controllers;

import controller.DatabaseRunner;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.XYChart;
import javafx.util.Pair;
import model.Service;
import model.metrics.ServiceMetrics;
import model.WeatherForecast;
import model.TimeOfDay;
import view.ChartType;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javafx.collections.FXCollections.observableArrayList;

/**
* The {@code WindowController} class mediates window elements.
*
* @author Georgij Krajnyukov
* @version 0.2.4
*/
public class WindowController implements Initializable, EventHandler<ApplicationEvent> {
    /**
    * controller of chart pane
    */
    @FXML
    private ChartController chartController;

    /**
    * controller of settings pane
    */
    @FXML
    private SettingsController settingsController;

    /**
     * controller of menu bar
     */
    @FXML
    private MenuBarController menuBarController;

    /**
    * map of {@link ServiceMetrics} used to plot chart with corresponding {@link ChartType}
    */
    private Map<ChartType, ServiceMetrics> metricsMap;

    /**
    * Constructs new instance.
    */
    public WindowController() {}

    /**
    * Sets service metrics map
    *
    * @param metricsMap map of {@link ServiceMetrics} used to plot chart with
    * corresponding {@link ChartType}
    */
    public void setMetricsMap(Map<ChartType, ServiceMetrics> metricsMap) {
        this.metricsMap = metricsMap;
    }

    /**
    * Initializes this instance. Called after all components are created.
    * Binds components together: adds self as listener of {@link ApplicationEvent}
    * to other controllers components.
    */
    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        settingsController.chartTypeComboBox.addEventHandler(ApplicationEvent.SETTINGS_CHANGED,
            event -> chartController.yAxis.setLabel(resourceBundle.getString(
            "chart.yAxis." + settingsController.getChartType()
        )));

        settingsController.chartTypeComboBox.addEventHandler(ApplicationEvent.SETTINGS_CHANGED, this);
        settingsController.startDate.addEventHandler(ApplicationEvent.SETTINGS_CHANGED, this);
        settingsController.endDate.addEventHandler(ApplicationEvent.SETTINGS_CHANGED, this);
        settingsController.timeOfDayComboBox.addEventHandler(ApplicationEvent.SETTINGS_CHANGED, this);

        settingsController.dateDifferenceSpinner.valueProperty().addListener((observableValue, integer, t1)
            -> this.handle(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED)));

        settingsController.yandexWeatherCheckBox.addEventHandler(ApplicationEvent.SETTINGS_CHANGED, this);
        settingsController.gismeteoCheckBox.addEventHandler(ApplicationEvent.SETTINGS_CHANGED, this);
        settingsController.accuweatherCheckBox.addEventHandler(ApplicationEvent.SETTINGS_CHANGED, this);
        settingsController.forecaCheckBox.addEventHandler(ApplicationEvent.SETTINGS_CHANGED, this);

        chartController.yAxis.setLabel(resourceBundle.getString("chart.yAxis."
            + settingsController.getChartType()));
    }

    /**
     * Pairs sorted by dates forecasts if their dates match. Both key and value
     * in these pairs are unique. That is, if any collection contains more forecasts
     * issued on particular date than other collection, they will be missed.
     *
     * @param forecastedData sorted by dates key forecasts
     * @param actualData sorted by dates value forecasts
     *
     * @return collection of sorted by dates pairs with key and value forecasted dates
     * matching
     */
    private List<Pair<WeatherForecast, WeatherForecast>> pairByDates(
                                                    List<WeatherForecast> forecastedData,
                                                    List<WeatherForecast> actualData) {
        List<Pair<WeatherForecast, WeatherForecast>> result = new LinkedList<>();

        var forecastedWeatherIterator = forecastedData.listIterator();
        var actualWeatherIterator = actualData.listIterator();

        while (forecastedWeatherIterator.hasNext() && actualWeatherIterator.hasNext()) {
            WeatherForecast forecastedWeather = forecastedWeatherIterator.next();
            WeatherForecast actualWeather = actualWeatherIterator.next();

            LocalDate forecastedWeatherDate = forecastedWeather.getForecastDate();
            LocalDate actualWeatherDate = actualWeather.getForecastDate();

            int distance = forecastedWeatherDate.compareTo(actualWeatherDate);

            //While dates are not matching and any matches could be found,
            //adjust iterators
            while (distance != 0 && forecastedWeatherIterator.hasNext()
                && actualWeatherIterator.hasNext()) {
                //If forecasted weather date is earlier, advance forecasted weather iterator
                if (distance < 0)
                    forecastedWeather = forecastedWeatherIterator.next();
                //If actual weather date is earlier, advance forecasted weather iterator
                else
                    actualWeather = actualWeatherIterator.next();

                forecastedWeatherDate = forecastedWeather.getForecastDate();
                actualWeatherDate = actualWeather.getForecastDate();
                distance = forecastedWeatherDate.compareTo(actualWeatherDate);
            }

            //If match is found earlier than iterators are reached the end
            if (distance == 0) {
                result.add(new Pair<>(forecastedWeather, actualWeather));
            }
        }

        return result;
    }

    /**
    * Sets data to a series using given {@code ServiceMetrics}.
    * For any date within specified range:
    * - x-axis tick mark is created
    * - if data is present, creates a bar with value of {@code ServiceMetrics::estimate}
    * with first parameter is key, second parameter is value
    * - if data is not present, bar is created with value 0
    *
    * @param series series to set data to
    * @param startDate the first date of specified range
    * @param endDate the last date of specified range
    * @param data collection of sorted by dates pairs with key and value forecasted dates
    * matching
    * @param metrics {@code ServiceMetrics} to use to generate data
    *
    * @see ServiceMetrics
    */
    private void setData(XYChart.Series<String, Float> series,
                         LocalDate startDate,
                         LocalDate endDate,
                         List<Pair<WeatherForecast, WeatherForecast>> data,
                         ServiceMetrics metrics) {
        ObservableList<XYChart.Data<String, Float>> newData = observableArrayList();

        LocalDate currentDate = startDate;
        for (var pair : data) {
            WeatherForecast forecastedWeather = pair.getKey();
            WeatherForecast actualWeather = pair.getValue();

            LocalDate forecastedDate = forecastedWeather.getForecastDate();
            for (LocalDate date = currentDate;
                           date.compareTo(forecastedDate) < 0;
                           date = date.plusDays(1)) {
                newData.add(new XYChart.Data<>(date.toString(), 0.0f));
            }

            newData.add(new XYChart.Data<>(forecastedDate.toString(),
                metrics.estimate(forecastedWeather, actualWeather)));

            currentDate = forecastedDate.plusDays(1);
        }

        for (LocalDate date = currentDate;
                       date.compareTo(endDate) <= 0;
                       date = date.plusDays(1)) {
            newData.add(new XYChart.Data<>(date.toString(), 0.0f));
        }

        series.setData(newData);
    }

    /**
    * Updates chart data depending on current settings.
    * For any date within selected range:
    * - x-axis tick mark is created
    * - if actual data is not present, no bars are created
    * - if service forecast is not present, only that service bar is not created
    *
    * @see SettingsController
    */
    private void updateData() {
        ChartType currentChartType = settingsController.getChartType();
        LocalDate currentStartDate = settingsController.getStartDate();
        LocalDate currentEndDate = settingsController.getEndDate();
        TimeOfDay currentTimeOfDay = settingsController.getTimeOfDay();
        Integer currentDateDifference = settingsController.getDateDifference();
        List<Service> currentServices = settingsController.getServices();

        var seriesMap = chartController.seriesMap;
        ServiceMetrics metrics = metricsMap.get(currentChartType);
        EnumMap<Service, Float> estimations = new EnumMap<>(Service.class);

        List<WeatherForecast> actualData = DatabaseRunner.getWeatherForecastRange(
                Service.ACTUAL, currentStartDate, currentEndDate, currentTimeOfDay, 0);

        for (Service service : seriesMap.keySet()) {
            var series = seriesMap.get(service);

            if (service == Service.ACTUAL) {
                if (currentServices.isEmpty() || currentChartType == ChartType.GENERAL) {
                    series.getData().clear();
                } else {
                    var matches = pairByDates(actualData, actualData);
                    setData(series, currentStartDate, currentEndDate, matches, metrics);
                }
                continue;
            }

            if (!currentServices.contains(service)) {
                series.getData().clear();
                continue;
            }

            try {
                List<WeatherForecast> forecastedData = DatabaseRunner.getWeatherForecastRange(
                    service, currentStartDate, currentEndDate, currentTimeOfDay,
                    currentDateDifference);

                var matches = pairByDates(forecastedData, actualData);

                setData(series, currentStartDate, currentEndDate, matches, metrics);
                estimations.put(service, metrics.estimate(matches));
            } catch (Exception e) {
                currentServices.remove(service);

                StringBuilder logMessageBuilder = new StringBuilder(
                    "unable to update data with current settings:\n");

                logMessageBuilder.append("\tchart type: ").append(currentChartType).append("\n");
                logMessageBuilder.append("\tstart date: ")
                    .append(currentStartDate.format(DateTimeFormatter.ISO_DATE)).append("\n");
                logMessageBuilder.append("\tend date: ")
                    .append(currentEndDate.format(DateTimeFormatter.ISO_DATE)).append("\n");
                logMessageBuilder.append("\ttime of day: ").append(currentTimeOfDay).append("\n");
                logMessageBuilder.append("\tdate difference: ").append(currentDateDifference).append("\n");
                logMessageBuilder.append("\tservice: ").append(service).append("\n");

                Logger.getLogger(getClass().getName())
                    .log(Level.WARNING, logMessageBuilder.toString(), e);
            }
        }

        currentServices.sort((service0, service1)
            -> (int) Math.signum(estimations.get(service1) - estimations.get(service0)));
        chartController.setChartInfoServicesTop(currentServices);
    }

    public void setOnLanguageChanged(EventHandler<LanguageChangedEvent> eventHandler) {
        menuBarController.menuBar.addEventHandler(LanguageChangedEvent.CHANGED, eventHandler);
    }

    /**
    * Handles {@code ApplicationEvent}'s.
    *
    * @param event {@code ApplicationEvent} to handle
    *
    * @see ApplicationEvent
    */
    @Override
    public void handle(ApplicationEvent event) {
        if (event.getEventType() == ApplicationEvent.SETTINGS_CHANGED) {
            updateData();
            event.consume();
        }
    }
}
