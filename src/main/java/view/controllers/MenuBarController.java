package view.controllers;

import controller.DatabaseRunner;
import controller.RaspisaniyePogodiParser;
import controller.WeatherForecastCollector;
import controller.receivers.AccuWeatherForecastReceiver;
import controller.receivers.ForecaForecastReceiver;
import controller.receivers.GismeteoForecastReceiver;
import controller.receivers.YandexWeatherForecastReceiver;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Menu;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.WeatherForecast;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;

import java.awt.*;
import java.net.URI;
import java.util.Locale;
import java.util.ResourceBundle;

public class MenuBarController implements Initializable {
    @FXML
    MenuBar menuBar;
    @FXML
    MenuItem englishMenuItem;
    @FXML
    MenuItem russianMenuItem;
    @FXML
    Label aboutMenu;

    @FXML
    Menu collectingMenu;

    private final Locale ENGLISH_LOCALE = new Locale("en", "UK");

    private final Locale RUSSIAN_LOCALE = new Locale("ru", "RU");

    private ResourceBundle resourceBundle;

    private boolean collecting = false;

    private final double ABOUT_WINDOW_WIDTH = 420;

    private final double ABOUT_WINDOW_HEIGHT = 300;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    public void switchToEnglish() {
        menuBar.fireEvent(new LanguageChangedEvent(LanguageChangedEvent.CHANGED, ENGLISH_LOCALE));
    }

    public void switchToRussian() {
        menuBar.fireEvent(new LanguageChangedEvent(LanguageChangedEvent.CHANGED, RUSSIAN_LOCALE));
    }

    public void onRegularCollecting() {
        if (collecting) {
            onCollectingAlreadyStarted();
            return;
        }
        collecting = true;

        WeatherForecastCollector collector = new WeatherForecastCollector(10,
                new YandexWeatherForecastReceiver(), new GismeteoForecastReceiver(),
                new AccuWeatherForecastReceiver(), new ForecaForecastReceiver()
        );

        Thread collectingThread = new Thread(() -> {
            try {
                List<WeatherForecast> weatherForecasts = collector.collect();
                DatabaseRunner.addWeatherForecastRange(weatherForecasts);
            } catch (Exception e) {
                collecting = false;
                Platform.runLater(this::onCollectingUnexpectedError);
                return;
            }
            collecting = false;
            Platform.runLater(this::onCollectingDone);
        });
        collectingThread.start();
    }

    public void onActualCollecting() {
        if (collecting) {
            onCollectingAlreadyStarted();
            return;
        }
        String filename = showCsvFileChooser();
        if (filename == null)
            return;

        collecting = true;

        Thread collectingThread = new Thread(() -> {
            try {
                List<WeatherForecast> actualData = RaspisaniyePogodiParser
                        .parseCsv(filename);
                DatabaseRunner.addWeatherForecastRange(actualData);
            } catch (IOException e) {
                collecting = false;
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR,
                        resourceBundle.getString("alert.collecting.iOError.message"),
                        ButtonType.OK);
                    alert.setTitle(resourceBundle.getString("alert.title"));
                    alert.setHeaderText(resourceBundle.getString("alert.title"));
                    alert.showAndWait();
                });
                return;
            } catch (Exception e) {
                collecting = false;
                Platform.runLater(this::onCollectingUnexpectedError);
                return;
            }
            collecting = false;
            Platform.runLater(this::onCollectingDone);
        });
        collectingThread.start();
    }

    private void onCollectingAlreadyStarted() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION,
            resourceBundle.getString("alert.collecting.alreadyStartedMessage"),
            ButtonType.OK);
        alert.setTitle(resourceBundle.getString("alert.information.title"));
        alert.setHeaderText(resourceBundle.getString("alert.information.title"));
        alert.showAndWait();
    }

    private void onCollectingDone() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION,
                resourceBundle.getString("alert.collecting.successMessage"),
                ButtonType.OK);
        alert.setTitle(resourceBundle.getString("alert.information.title"));
        alert.setHeaderText(resourceBundle.getString("alert.information.title"));
        alert.showAndWait();
    }

    private void onCollectingUnexpectedError() {
        Alert alert = new Alert(Alert.AlertType.ERROR,
            resourceBundle.getString("alert.unexpectedError.message"),
            ButtonType.OK);
        alert.setTitle(resourceBundle.getString("alert.title"));
        alert.setHeaderText(resourceBundle.getString("alert.title"));
        alert.showAndWait();
    }

    private String showCsvFileChooser() {
        Stage fileChooserStage = new Stage();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));

        File file = fileChooser.showOpenDialog(fileChooserStage);

        fileChooserStage.setWidth(300);
        fileChooserStage.setHeight(300);
        fileChooserStage.show();
        if (file == null) {
            fileChooserStage.close();
            return null;
        }
        fileChooserStage.close();

        return file.getAbsolutePath();
    }

    public void openAboutMenu() {
        Stage aboutMenuStage = new Stage();

        VBox aboutMenuVbox = new VBox();

        Label repositoryLabel = new Label(resourceBundle.getString("about.repository.label"));
        repositoryLabel.setFont(new Font("Calibri", 20));
        Label gitlabLabel = new Label(resourceBundle.getString("about.gitlab"));
        gitlabLabel.setFont(new Font("Calibri", 14));

        Label datasourcesLabel = new Label(resourceBundle.getString("about.datasources.label"));
        datasourcesLabel.setFont(new Font("Calibri", 20));
        Label yandexWeatherLabel = new Label(resourceBundle.getString("about.YandexWeather"));
        yandexWeatherLabel.setFont(new Font("Calibri", 14));
        Label gismeteoLabel = new Label(resourceBundle.getString("about.gismeteo"));
        gismeteoLabel.setFont(new Font("Calibri", 14));
        Label accuWeatherLabel = new Label(resourceBundle.getString("about.accuweather"));
        accuWeatherLabel.setFont(new Font("Calibri", 14));
        Label forecaLabel = new Label(resourceBundle.getString("about.foreca"));
        forecaLabel.setFont(new Font("Calibri", 14));
        Label rp5Label = new Label(resourceBundle.getString("about.rp5"));
        rp5Label.setFont(new Font("Calibri", 14));

        Hyperlink gitlabLink = new Hyperlink("https://bit.ly/3GZff5c");
        Hyperlink yandexWeatherLink = new Hyperlink("https://yandex.com/weather/");
        Hyperlink gismeteoLink = new Hyperlink("https://www.gismeteo.com/");
        Hyperlink accuWeatherLink = new Hyperlink("https://www.accuweather.com/");
        Hyperlink forecaLink = new Hyperlink("https://www.foreca.com/");
        Hyperlink rp5Link = new Hyperlink("https://bit.ly/3J68W1E");

        gitlabLink.setGraphic(gitlabLabel);
        yandexWeatherLink.setGraphic(yandexWeatherLabel);
        gismeteoLink.setGraphic(gismeteoLabel);
        accuWeatherLink.setGraphic(accuWeatherLabel);
        forecaLink.setGraphic(forecaLabel);
        rp5Link.setGraphic(rp5Label);

        gitlabLink.setOnAction(e -> {
            try {
                Desktop.getDesktop().browse(URI.create(gitlabLink.getText()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        yandexWeatherLink.setOnAction(e -> {
            try {
                Desktop.getDesktop().browse(URI.create(yandexWeatherLink.getText()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        gismeteoLink.setOnAction(e -> {
            try {
                Desktop.getDesktop().browse(URI.create(gismeteoLink.getText()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        accuWeatherLink.setOnAction(e -> {
            try {
                Desktop.getDesktop().browse(URI.create(accuWeatherLink.getText()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        forecaLink.setOnAction(e -> {
            try {
                Desktop.getDesktop().browse(URI.create(forecaLink.getText()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        rp5Link.setOnAction(e -> {
            try {
                Desktop.getDesktop().browse(URI.create(rp5Link.getText()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        aboutMenuVbox.getChildren().addAll(repositoryLabel, gitlabLink, datasourcesLabel,yandexWeatherLink,
                gismeteoLink, accuWeatherLink, forecaLink, rp5Link);
        aboutMenuVbox.setSpacing(5);
        aboutMenuVbox.setAlignment(Pos.CENTER);

        BorderPane aboutMenuBorderPane = new BorderPane();
        aboutMenuBorderPane.setCenter(aboutMenuVbox);

        aboutMenuStage.setTitle(resourceBundle.getString("about.title"));
        aboutMenuStage.setWidth(ABOUT_WINDOW_WIDTH);
        aboutMenuStage.setHeight(ABOUT_WINDOW_HEIGHT);
        aboutMenuStage.setResizable(false);
        aboutMenuStage.initModality(Modality.APPLICATION_MODAL);
        aboutMenuStage.setScene(new Scene(aboutMenuBorderPane));
        aboutMenuStage.show();
    }
}
