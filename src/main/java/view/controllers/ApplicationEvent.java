package view.controllers;

import javafx.event.Event;
import javafx.event.EventType;

/**
* The {@code ApplicationEvent} class is an {@code Event}
* related to main application.
*
* @author Georgij Krajnyukov
* @version 0.1.0
*/
public class ApplicationEvent extends Event {
    /**
    * type of event that occurs when chart settings changed
    */
    public static final EventType<ApplicationEvent> SETTINGS_CHANGED =
        new EventType<>(ANY, "SETTINGS_CHANGED");

    /**
    * Construct new {@code ApplicationEvent} with the specified event type.
    *
    * @param eventType an event type
    */
    public ApplicationEvent(EventType<? extends Event> eventType) {
        super(eventType);
    }


}
