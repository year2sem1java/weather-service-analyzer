package view.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import model.Service;
import model.TimeOfDay;
import view.ChartType;

import java.net.URL;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

/**
* The {@code SettingsController} manages settings panel components.
*
* @author Georgij Krajnyukov
* @version 0.1.2
*/
public class SettingsController implements Initializable {
    /**
    * chart type selection component
    */
    @FXML
    ComboBox<ChartType> chartTypeComboBox;

    /**
    * start date selection component
    */
    @FXML
    DatePicker startDate;

    /**
    * end date selection component
    */
    @FXML
    DatePicker endDate;

    /**
    * time of day selection component
    */
    @FXML
    ComboBox<TimeOfDay> timeOfDayComboBox;

    /**
    * date difference selection component
    */
    @FXML
    Spinner<Integer> dateDifferenceSpinner;

    /**
    * Yandex.Weather selection component
    */
    @FXML
    CheckBox yandexWeatherCheckBox;

    /**
    * Gismeteo selection component
    */
    @FXML
    CheckBox gismeteoCheckBox;

    /**
    * AccuWeather selection component
    */
    @FXML
    CheckBox accuweatherCheckBox;

    /**
    * Foreca selection component
    */
    @FXML
    CheckBox forecaCheckBox;

    /**
    * Constructs new instance.
    */
    public SettingsController() {}

    /**
    * Initializes this instance. Called after all components are created.
    * Sets default values of settings components. Does not update the chart.
    */
    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        chartTypeComboBox.setCellFactory(p -> new ListCell<>() {
            @Override
            protected void updateItem(ChartType chartType, boolean empty) {
                super.updateItem(chartType, empty);

                if (chartType == null || empty)
                    return;

                setText(resourceBundle.getString("chartTypeComboBox." + chartType));
            }
        });
        chartTypeComboBox.setButtonCell(new ListCell<>() {
            @Override
            protected void updateItem(ChartType chartType, boolean empty) {
                super.updateItem(chartType, empty);

                if (chartType == null || empty)
                    return;

                setText(resourceBundle.getString("chartTypeComboBox." + chartType));
            }
        });

        timeOfDayComboBox.setCellFactory(p -> new ListCell<>() {
            @Override
            protected void updateItem(TimeOfDay timeOfDay, boolean empty) {
                super.updateItem(timeOfDay, empty);

                if (timeOfDay == null || empty)
                    return;

                setText(resourceBundle.getString("timeOfDayComboBox." + timeOfDay));
            }
        });
        timeOfDayComboBox.setButtonCell(new ListCell<>() {
            @Override
            protected void updateItem(TimeOfDay timeOfDay, boolean empty) {
                super.updateItem(timeOfDay, empty);

                if (timeOfDay == null || empty)
                    return;

                setText(resourceBundle.getString("timeOfDayComboBox." + timeOfDay));
            }
        });

        LocalDate now = LocalDate.now();

        chartTypeComboBox.setValue(ChartType.GENERAL);
        startDate.setValue(now.minusDays(7));
        endDate.setValue(now);
        timeOfDayComboBox.setValue(TimeOfDay.AFTERNOON);
        dateDifferenceSpinner.getValueFactory().setValue(0);
    }

    /**
    * Reacts on change of chart type.
    */
    @FXML
    private void onChartTypeChanged() {
        chartTypeComboBox.fireEvent(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED));
    }

    /**
    * Reacts on change of start date.
    */
    @FXML
    private void onStartDateChanged() {
        startDate.fireEvent(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED));
    }

    /**
    * Reacts on change of end date.
    */
    @FXML
    private void onEndDateChanged() {
        endDate.fireEvent(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED));
    }

    /**
    * Reacts on change of time of day.
    */
    @FXML
    private void onTimeOfDayChanged() {
        timeOfDayComboBox.fireEvent(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED));
    }

    /**
    * Reacts on selection of Yandex.Weather.
    */
    @FXML
    private void onYandexWeatherSelected() {
        yandexWeatherCheckBox.fireEvent(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED));
    }

    /**
    * Reacts on selection of Gismeteo.
    */
    @FXML
    private void onGismeteoSelected() {
        gismeteoCheckBox.fireEvent(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED));
    }

    /**
    * Reacts on selection of AccuWeather.
    */
    @FXML
    private void onAccuweatherSelection() {
        accuweatherCheckBox.fireEvent(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED));
    }

    /**
    * Reacts on selection of Foreca.
    */
    @FXML
    private void onForecaSelected() {
        forecaCheckBox.fireEvent(new ApplicationEvent(ApplicationEvent.SETTINGS_CHANGED));
    }

    /**
    * Gets selected chart type.
    *
    * @return selected chart type
    */
    ChartType getChartType() {
        return chartTypeComboBox.getValue();
    }

    /**
    * Gets selected start date.
    *
    * @return selected start date
    */
    LocalDate getStartDate() {
        return startDate.getValue();
    }

    /**
    * Gets selected end date.
    *
    * @return selected end date
    */
    LocalDate getEndDate() {
        return endDate.getValue();
    }

    /**
    * Gets selected time of day.
    *
    * @return selected time of day
    */
    TimeOfDay getTimeOfDay() {
        return timeOfDayComboBox.getValue();
    }

    /**
    * Gets selected date difference.
    *
    * @return selected date difference
    */
    Integer getDateDifference() {
        return dateDifferenceSpinner.getValue();
    }

    /**
    * Gets selected services.
    *
    * @return collection of selected services
    */
    List<Service> getServices() {
        List<Service> services = new LinkedList<>();

        if (yandexWeatherCheckBox.isSelected())
            services.add(Service.YANDEX_WEATHER);
        if (gismeteoCheckBox.isSelected())
            services.add(Service.GISMETEO);
        if (accuweatherCheckBox.isSelected())
            services.add(Service.ACCUWEATHER);
        if (forecaCheckBox.isSelected())
            services.add(Service.FORECA);

        return services;
    }
}
