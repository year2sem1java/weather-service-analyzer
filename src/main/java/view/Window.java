package view;

import controller.DatabaseRunner;
import controller.WeatherForecastCollector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import model.metrics.*;
import view.controllers.WindowController;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.EnumMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@code Window} Initializes and visualizes the application interface.
 *
 * @author Maxim Sergeev
 * @author Georgij Krajnyukov
 * @version 0.4.5
 */
public class Window extends Application {
    /**
     * Minimum width that a window can accept.
     */
    private final double MINIMUM_SCREEN_WIDTH = 800;

    /**
     * Minimum height that a window can accept.
     */
    private final double MINIMUM_SCREEN_HEIGHT = 600;

    /**
     * Default value for window width.
     */
    private final double SCREEN_WIDTH = 1024;

    /**
     * Default value for window height.
     */
    private final double SCREEN_HEIGHT = 768;

    private final Locale DEFAULT_LOCALE = new Locale("ru", "RU");

    private final URL WINDOW_RESOURCES_URL = getClass().getResource("../fxml/Window.fxml");

    private final Map<ChartType, ServiceMetrics> metricsMap = new EnumMap<>(ChartType.class);

    /**
     * Defines the application interface.
     *
     * @param stage primary object of class {@link Stage}. Constructed by the platform.
     *
     * @throws IOException if load resource URL is unsuccessful.
     * @throws NullPointerException if path to resource catalog is null
     */
    @Override
    public void start(Stage stage) throws IOException, NullPointerException {
        Path logsPath = Path.of("logs/");
        if (!Files.exists(logsPath)) {
            try {
                Files.createDirectory(logsPath);
            } catch (IOException e) {
                Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
                    .log(Level.WARNING, "unable to create logs directory", e);
            }
        }

        LocalDateTime now = LocalDateTime.now();
        String nowString = now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd-hh-mm-ss"));

        try {
            Logger windowLogger = Logger.getLogger(Window.class.getName());
            windowLogger.setLevel(Level.INFO);

            FileHandler windowFileHandler = new FileHandler(logsPath.resolve(
                String.format("%s-%s.log", Window.class.getName(), nowString)).toString());
            windowFileHandler.setLevel(Level.WARNING);
            windowLogger.addHandler(windowFileHandler);
        } catch (IOException e) {
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
                    .log(Level.WARNING, "unable to create application log file", e);
        }

        try {
            Logger windowControllerLogger = Logger.getLogger(WindowController.class.getName());
            windowControllerLogger.setLevel(Level.WARNING);

            FileHandler windowControllerFileHandler = new FileHandler(logsPath.resolve(
                String.format("%s-%s.log", WindowController.class.getName(), nowString)).toString());
            windowControllerFileHandler.setLevel(Level.WARNING);
            windowControllerLogger.addHandler(windowControllerFileHandler);
        } catch (IOException e) {
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
                    .log(Level.WARNING, "unable to create application log file", e);
        }

        try {
            Logger databaseLogger = Logger.getLogger(DatabaseRunner.class.getName());
            databaseLogger.setLevel(Level.INFO);

            FileHandler databaseFileHandler = new FileHandler(logsPath.resolve(
                String.format("%s-%s.log", DatabaseRunner.class.getName(), nowString)).toString());
            databaseFileHandler.setLevel(Level.WARNING);
            databaseLogger.addHandler(databaseFileHandler);
        } catch (IOException e) {
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
                    .log(Level.WARNING, "unable to create database log file", e);
        }

        try {
            Logger collectingLogger = Logger.getLogger(WeatherForecastCollector.class.getName());
            collectingLogger.setLevel(Level.INFO);

            FileHandler collectingFileHandler = new FileHandler(logsPath.resolve(
                String.format("%s-%s.log", WeatherForecastCollector.class.getName(), nowString)).toString());
            collectingFileHandler.setLevel(Level.WARNING);
            collectingLogger.addHandler(collectingFileHandler);
        } catch (IOException e) {
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
                    .log(Level.WARNING, "unable to create collecting log file", e);
        }

        try {
            DatabaseRunner.start();
        } catch(Exception e) {
            Logger.getLogger(Window.class.getName()).log(Level.SEVERE,
                "unable to start database runner", e);

            ResourceBundle bundle = ResourceBundle.getBundle("bundles.strings", DEFAULT_LOCALE);

            Alert alert = new Alert(Alert.AlertType.ERROR, bundle.getString("alert.contentText"),
                    ButtonType.OK);
            alert.setTitle(bundle.getString("alert.title"));
            alert.setHeaderText(bundle.getString("alert.headerText"));
            alert.showAndWait();
            return;
        }

        //Set application window title and size
        stage.setWidth(SCREEN_WIDTH);
        stage.setHeight(SCREEN_HEIGHT);
        stage.setMinWidth(MINIMUM_SCREEN_WIDTH);
        stage.setMinHeight(MINIMUM_SCREEN_HEIGHT);

        metricsMap.put(ChartType.TEMPERATURE, new TemperatureServiceMetrics());
        metricsMap.put(ChartType.HUMIDITY, new HumidityServiceMetrics());
        metricsMap.put(ChartType.WIND_SPEED, new WindSpeedServiceMetrics());
        metricsMap.put(ChartType.GENERAL, new GeneralServiceMetrics());

        loadScene(stage, DEFAULT_LOCALE);

        stage.setOnCloseRequest(windowEvent -> DatabaseRunner.finish());

        stage.show();
    }

    private void loadScene(Stage stage, Locale locale) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(WINDOW_RESOURCES_URL);
        fxmlLoader.setResources(ResourceBundle.getBundle("bundles.strings", locale));

        stage.setTitle(fxmlLoader.getResources().getString("title"));

        Parent root = fxmlLoader.load();
        Scene scene = stage.getScene();
        if (scene == null)
            stage.setScene(new Scene(root));
        else
            scene.setRoot(root);

        WindowController windowController = fxmlLoader.getController();

        windowController.setMetricsMap(metricsMap);
        windowController.setOnLanguageChanged(languageChangedEvent -> {
            try {
                loadScene(stage, languageChangedEvent.getLocale());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
