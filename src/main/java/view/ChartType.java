package view;

/**
* The {@code ChartType} enum represents types of chart.
*
* @author Georgij Krajnyukov
* @version 0.1.0
*/
public enum ChartType {
    /**
    * chart that shows temperature discrepancy
    */
    TEMPERATURE,
    /**
    * chart that shows humidity discrepancy
    */
    HUMIDITY,
    /**
    * chart that shows wind speed discrepancy
    */
    WIND_SPEED,
    /**
    * chart that shows discrepancy of all parameters
    */
    GENERAL,
}
