package controller;

import controller.receivers.ForecastReceiver;
import model.TimeOfDay;
import model.WeatherForecast;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
* The {@code WeatherForecastCollector} collects forecast data within
* specified date range.
*
* @author Georgij Krajnyukov
* @version 0.1.2
*/
public class WeatherForecastCollector {
    /**
    * collection of receivers
    */
    private final List<ForecastReceiver> receivers;

    /**
    * maximum date difference
    */
    private final int maxDateDifference;

    /**
    * Constructs {@code WeatherForecastCollector} collecting
    * forecasts for {@code maxDateDifference} days ahead.
    *
    * @param maxDateDifference maximum number of days from today for
    * which data would be collected
    * @param receivers target receivers
    */
    public WeatherForecastCollector(int maxDateDifference, ForecastReceiver ... receivers) {
        this.maxDateDifference = maxDateDifference;
        this.receivers = List.of(receivers);
    }

    /**
    * Returns collected forecast data. Iterating through receivers, tries to get
    * forecasts data for each time of the day, so if exception are thrown,
    * only particular time of the day will not be included.
    *
    * @return collection of collected data
    */
    public List<WeatherForecast> collect() {
        List<WeatherForecast> weatherForecasts = new LinkedList<>();
        LocalDate now = LocalDate.now();

        Logger collectingLogger = Logger.getLogger(getClass().getName());

        for (ForecastReceiver receiver : receivers) {
            for (int dayDifference = 0; dayDifference < maxDateDifference; ++dayDifference) {
                for (TimeOfDay timeOfDay : TimeOfDay.values()) {
                    LocalDate forecastedDate = now.plusDays(dayDifference);
                    try {
                        WeatherForecast weatherForecast = receiver.getWeatherForecast(
                            timeOfDay, forecastedDate);
                        weatherForecasts.add(weatherForecast);
                        collectingLogger.log(Level.INFO, "successful collecting: "
                            + weatherForecast);
                    } catch (RuntimeException e) {
                        collectingLogger.log(Level.WARNING, "unsuccessful collecting on "
                            + forecastedDate + ", " + timeOfDay, e);
                    }
                }
            }
        }
        return weatherForecasts;
    }
}
