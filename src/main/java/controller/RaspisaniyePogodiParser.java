package controller;

import model.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class provides means to parse Raspisaniye Pogodi service data.
 *
 * @author Georgij Krajnyukov
 * @version 0.1.0
 */
public class RaspisaniyePogodiParser {
    /**
     * Returns wind direction model representation value of the Raspisaniye Pogodi wind
     * direction description.
     *
     * <b>Warning: present value "Переменное направление" are not supported.</b>
     *
     * @param windDirectionDescription Raspisaniye Pogodi description of a wind direction
     *
     * @return a wind direction model representation of the Raspisaniye Pogodi wind
     * direction description
     *
     * @throws IllegalArgumentException if any unsupported value is passed
     */
    private static WindDirection parseWindDirection(String windDirectionDescription) {
        windDirectionDescription = windDirectionDescription.toLowerCase(Locale.ROOT);

        if (windDirectionDescription.contains("безветрие"))
            return WindDirection.NONE;
        else if (windDirectionDescription.contains("северо-восток"))
            return WindDirection.NORTHEAST;
        else if (windDirectionDescription.contains("северо-запад"))
            return WindDirection.NORTHWEST;
        else if (windDirectionDescription.contains("юго-восток"))
            return WindDirection.SOUTHEAST;
        else if (windDirectionDescription.contains("юго-запад"))
            return WindDirection.NORTHWEST;
        else if (windDirectionDescription.contains("север"))
            return WindDirection.NORTH;
        else if (windDirectionDescription.contains("восток"))
            return WindDirection.EAST;
        else if (windDirectionDescription.contains("юг"))
            return WindDirection.SOUTH;
        else if (windDirectionDescription.contains("запад"))
            return WindDirection.WEST;
        throw new IllegalArgumentException("unable to parse wind direction description: "
            + windDirectionDescription);
    }

    /**
     * Returns weather situation model representation of Raspisaniye Pogodi weather
     * situation description.
     *
     * @param weatherSituation Raspisaniye Pogodi weather situation description
     *
     * @return a weather situation model representation of Raspisaniye Pogodi weather
     * situation description
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private static WeatherSituation parseWeatherSituation(String weatherSituation,
                                                          String cloudinessDescription) {
        weatherSituation = weatherSituation.toLowerCase(Locale.ROOT);
        cloudinessDescription = cloudinessDescription.toLowerCase(Locale.ROOT);
        if (weatherSituation.contains("дождь") || weatherSituation.contains("ливень")
            || weatherSituation.contains("морось"))
            return WeatherSituation.RAIN;
        if (weatherSituation.contains("снег") || weatherSituation.contains("град")
            || weatherSituation.contains("снежная крупа")
            || weatherSituation.contains("ледяная крупа")
            || weatherSituation.contains("ледяные иглы"))
            return WeatherSituation.SNOW;
        if (weatherSituation.contains("гроза"))
            return WeatherSituation.THUNDERSTORM;
        if (weatherSituation.contains("туман") || weatherSituation.contains("дымка")
            || weatherSituation.contains("дым"))
            return WeatherSituation.FOG;

        if (cloudinessDescription.contains("кучево-дождевые облака")
            || cloudinessDescription.contains("рассеянная")
            || cloudinessDescription.contains("разорванная")
            || cloudinessDescription.contains("сплошная")
            || cloudinessDescription.contains("кучевые мощные большой вертикальной протяженности"))
            return WeatherSituation.CLOUDY;

        if (cloudinessDescription.equals("нет существенной облачности")
            || cloudinessDescription.contains("незначительная")
            || cloudinessDescription.contains("вертикальная видимость"))
            return WeatherSituation.CLEAR;

        throw new RuntimeException("unable to parse weather description: " + weatherSituation
            + " " + cloudinessDescription);
    }

    /**
     * Parses Raspisaniye Pogodi forecasts data presented in CSV format.
     *
     * @param filename name of a CSV file
     *
     * @return a collection of forecast model representations
     *
     * @throws IOException if any i/o errors occur while working with the file
     * @throws RuntimeException if any error occurs when parsing forecast data
     */
    public static List<WeatherForecast> parseCsv(String filename) throws IOException {
        List<WeatherForecast> result = new LinkedList<>();

        LocalTime midnightTime = LocalTime.MIDNIGHT;
        LocalTime morningTime = LocalTime.of(6, 0, 0);
        LocalTime afternoonTime = LocalTime.of(12, 0, 0);
        LocalTime eveningTime = LocalTime.of(18, 0, 0);
        Pattern rowPattern = Pattern.compile("\"(.*?)\";\"(.*?)\";\".*?\";\".*?\";\"(.*?)\";\"(.*?)\";\"(.*?)\";\".*?\";\"(.*?)\";\".*?\";\"(.*?)\";\".*?\";\".*?\";");
        try (BufferedReader reader = new BufferedReader(
                new FileReader(filename, StandardCharsets.UTF_8))) {
            String rowLine = reader.readLine();

            //Discard unnecessary data
            while (rowLine.startsWith("#"))
                rowLine = reader.readLine();

            //Discard table header
            rowLine = reader.readLine();

            while ((rowLine = reader.readLine()) != null) {
                try {
                    Matcher rowMatcher = rowPattern.matcher(rowLine);
                    if (!rowMatcher.find())
                        throw new RuntimeException("unable to parse a line " + rowLine);

                    LocalDateTime currentDateTime = LocalDateTime.parse(rowMatcher.group(1),
                            DateTimeFormatter.ofPattern("dd.MM.yyyy kk:mm"));
                    LocalTime currentTime = currentDateTime.toLocalTime();
                    TimeOfDay timeOfDay = null;

                    if (currentTime.compareTo(midnightTime) == 0) {
                        timeOfDay = TimeOfDay.MIDNIGHT;
                    } else if (currentTime.compareTo(morningTime) == 0) {
                        timeOfDay = TimeOfDay.MORNING;
                    } else if (currentTime.compareTo(afternoonTime) == 0) {
                        timeOfDay = TimeOfDay.AFTERNOON;
                    } else if (currentTime.compareTo(eveningTime) == 0) {
                        timeOfDay = TimeOfDay.EVENING;
                    } else {
                        continue;
                    }
                    LocalDate forecastDate = currentDateTime.toLocalDate();

                    float temperature = Float.parseFloat(rowMatcher.group(2));

                    int humidity = Integer.parseInt(rowMatcher.group(3));
                    WindDirection windDirection = parseWindDirection(rowMatcher.group(4));
                    float windSpeed = Float.parseFloat(rowMatcher.group(5));
                    WeatherSituation weatherSituation = parseWeatherSituation(
                            rowMatcher.group(6), rowMatcher.group(7));

                    result.add(new WeatherForecast.Builder()
                            .temperature(temperature)
                            .humidity(humidity)
                            .weatherSituation(weatherSituation)
                            .windSpeed(windSpeed)
                            .windDirection(windDirection)
                            .timeOfDay(timeOfDay)
                            .forecastDate(forecastDate)
                            .dateDifference(0)
                            .service(Service.ACTUAL)
                            .build());
                } catch (Exception e) {
                    Logger.getLogger(RaspisaniyePogodiParser.class.getName())
                            .log(Level.WARNING, "unable to parse: " + rowLine, e);
                }
            }
        }

        return result;
    }
}
