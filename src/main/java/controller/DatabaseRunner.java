package controller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.*;

/**
 * The {@code DatabaseRunner} class gives access to database.
 * It configures session and encapsulates work with database.
 *
 * @author Matwey Ivanov
 * @author Georgij Krajnyukov
 * @version 0.8.0
 */
public class DatabaseRunner {
    /**
     * database session factory
     */
    private static SessionFactory sessionFactory = null;

    /**
     * Initializes {@link #sessionFactory} from configuration file.
     */
    private static void initSessionFactory() {
        sessionFactory = new Configuration()
            .configure("\\hibernate\\hibernate.cfg.xml")
            .addAnnotatedClass(WeatherForecast.class)
            .buildSessionFactory();
    }

    /**
    * Constructs nothing.
    */
    private DatabaseRunner() {}

    /**
     * Starts database runner work. Required before accessing database.
     *
     * @throws IllegalStateException if runner already has been started
     */
    public static void start() {
        if(!Objects.isNull(sessionFactory))
            throw new IllegalStateException("database runner already has been started");
        initSessionFactory();
    }

    /**
     * Finishes database runner work. {@link #start()} is required to access
     * database again.
     */
    public static void finish() {
        sessionFactory.close();
    }

    /**
     * Adds record to database.
     *
     * @param weatherForecasts forecasts to add to database
     *
     * @throws IllegalStateException if database runner hasn't been started
     * @throws NullPointerException if any forecast or range itself is null.
     * All forecasts are not saved then.
     * @throws RuntimeException if any error occurs during database access
     */
    public static void addWeatherForecastRange(Iterable<WeatherForecast> weatherForecasts) {
        if (Objects.isNull(sessionFactory))
            throw new IllegalStateException("database runner hasn't been started");
        Objects.requireNonNull(weatherForecasts);

        Logger databaseLogger = Logger.getLogger(DatabaseRunner.class.getName());
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            for (WeatherForecast weatherForecast : weatherForecasts) {
                Objects.requireNonNull(weatherForecast);
                transaction = session.beginTransaction();
                try {
                    session.save(weatherForecast);
                    databaseLogger.log(Level.INFO, "New record added to database");
                    transaction.commit();
                } catch(ConstraintViolationException e) {
                    databaseLogger.log(Level.WARNING, "Attempt to add duplicate record", e);
                    transaction.rollback();
                }
            }

        } catch (Exception e) {
            if (!Objects.isNull(transaction)) transaction.rollback();

            databaseLogger.log(Level.SEVERE, "Unable to add data", e);
            throw new RuntimeException("unable to add data", e);
        } finally {
            session.close();
        }
    }

    /**
     * Gets forecast weather objects from database ordered by date.
     * Return objects with date from leftBound and to rightBound.
     *
     * @param service target service of forecast
     * @param fromDate first date of the range
     * @param toDate last date of the range
     * @param timeOfDay forecasted time of day
     * @param dateDifference number of days between forecasted date and issued date
     *
     * @throws IllegalStateException if database runner hasn't been started
     * @throws NullPointerException if some parameter is null
     * @throws IllegalArgumentException if date difference is negative
     * @throws RuntimeException if any error occurs during database access
     *
     * @return list of forecasts for these dates sorted by forecast date
     */
    public static List<WeatherForecast> getWeatherForecastRange(Service service,
                                                                LocalDate fromDate,
                                                                LocalDate toDate,
                                                                TimeOfDay timeOfDay,
                                                                int dateDifference) {
        if (Objects.isNull(sessionFactory))
            throw new IllegalStateException("database runner hasn't been started");

        Objects.requireNonNull(service);
        Objects.requireNonNull(fromDate);
        Objects.requireNonNull(toDate);
        Objects.requireNonNull(timeOfDay);

        if (dateDifference < 0)
            throw new IllegalArgumentException("date difference must not be negative");

        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query<WeatherForecast> weatherForecastQuery = session.createQuery(
                    "FROM WeatherForecast WHERE forecastDate >= :fromDate AND forecastDate <= :toDate " +
                            "AND timeOfDay = :timeOfDay AND dateDifference = :dateDifference AND service = :service "
                            + "ORDER BY forecastDate asc",
                       WeatherForecast.class);

            weatherForecastQuery.setParameter("fromDate", fromDate);
            weatherForecastQuery.setParameter("toDate", toDate);
            weatherForecastQuery.setParameter("timeOfDay", timeOfDay);
            weatherForecastQuery.setParameter("dateDifference", dateDifference);
            weatherForecastQuery.setParameter("service", service);

            List<WeatherForecast> result = weatherForecastQuery.list();

            transaction.commit();
            return result;
        } catch (Exception e) {
            if (!Objects.isNull(transaction)) transaction.rollback();
            throw new RuntimeException("unable to get data", e);
        } finally {
            session.close();
        }
    }
}
