package controller.receivers;

import model.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The {@code AccuWeatherForecastReceiver} class receives data from AccuWeather.
 *
 * @author Georgij Krajnyukov
 * @version 0.1.2
 */
public class AccuWeatherForecastReceiver implements ForecastReceiver {
    /**
     * URL to AccuWeather (Zelenograd)
     */
    private final String BASE_URL = "https://www.accuweather.com/en/ru/zelenograd/294018/";

    /**
     * a relative to {@code #BASE_URL} path leading to AccuWeather morning
     * forecasts
     *
     * @see #BASE_URL
     */
    private final String MORNING_CATEGORY = "morning-weather-forecast/294018";
    /**
     * a relative to {@code #BASE_URL} path leading to AccuWeather afternoon
     * forecasts
     *
     * @see #BASE_URL
     */
    private final String AFTERNOON_CATEGORY = "/afternoon-weather-forecast/294018";
    /**
     * a relative to {@code #BASE_URL} path leading to AccuWeather evening
     * forecasts
     *
     * @see #BASE_URL
     */
    private final String EVENING_CATEGORY = "/evening-weather-forecast/294018";
    /**
     * a relative to {@code #BASE_URL} path leading to AccuWeather overnight
     * forecasts
     *
     * @see #BASE_URL
     */
    private final String MIDNIGHT_CATEGORY = "/overnight-weather-forecast/294018";

    /**
     * Returns AccuWeather forecast web page's text. Makes a sync GET request
     * to AccuWeather based on the parameters.
     *
     * @param timeOfDay forecasted time of the day
     * @param dateDifference difference between current day of month and
     * the requested date's day of month
     *
     * @return AccuWeather forecast web page's text
     *
     * @throws RuntimeException if failed to get response
     */
    private String getHtmlBody(TimeOfDay timeOfDay, int dateDifference) {
        String requestString = switch (timeOfDay) {
            case MORNING -> MORNING_CATEGORY;
            case AFTERNOON -> AFTERNOON_CATEGORY;
            case EVENING -> EVENING_CATEGORY;
            case MIDNIGHT -> MIDNIGHT_CATEGORY;
        };
        requestString += String.format("?day=%d/", dateDifference + 1);

        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(BASE_URL + requestString))
                .GET()
                .build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != HttpURLConnection.HTTP_OK)
                throw new RuntimeException("bad request: " + requestString +  ". Status code: " + response.statusCode());
            return response.body();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("could not receive data from AccuWeather", e);
        }
    }

    /**
     * Returns model representation of AccuWeather weather situation description.
     *
     * @param weatherSituation AccuWeather weather situation description
     *
     * @return model representation of AccuWeather weather situation description
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private WeatherSituation parseWeatherSituation(String weatherSituation) {
        weatherSituation = weatherSituation.toLowerCase(Locale.ROOT);

        if (weatherSituation.contains("cloudy")
                || weatherSituation.contains("cloud")
                || weatherSituation.contains("partly")
                || weatherSituation.contains("dreary")
                || weatherSituation.contains("overcast")
                || weatherSituation.contains("flurr")) {
            return WeatherSituation.CLOUDY;
        }
        if (weatherSituation.contains("sunny") || weatherSituation.contains("clear")) {
            return WeatherSituation.CLEAR;
        }
        if (weatherSituation.contains("rain") || weatherSituation.contains("showers"))
            return WeatherSituation.RAIN;
        if (weatherSituation.contains("snow"))
            return WeatherSituation.SNOW;
        if (weatherSituation.contains("t-storms"))
            return WeatherSituation.THUNDERSTORM;
        if (weatherSituation.contains("fog") || weatherSituation.contains("hazy"))
            return WeatherSituation.FOG;

        throw new IllegalArgumentException("unknown weather situation: " + weatherSituation);
    }

    /**
     * Returns model representation of Gismeteo wind direction representation.
     *
     * @param windDirection AccuWeather representation of wind direction
     *
     * @return a {@link WindDirection} value of the AccuWeather wind
     * direction representation
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private WindDirection parseWindDirection(String windDirection) {
        if (windDirection.equals("N") || windDirection.equals("NNW")
                || windDirection.equals("NNE")) {
            return WindDirection.NORTH;
        }

        if (windDirection.equals("NW"))
            return WindDirection.NORTHWEST;
        if (windDirection.equals("NE"))
            return WindDirection.NORTHEAST;

        if (windDirection.equals("W") || windDirection.equals("WNW")
                || windDirection.equals("WSW")) {
            return WindDirection.WEST;
        }

        if (windDirection.equals("S") || windDirection.equals("SSW")
                || windDirection.equals("SSE")) {
            return WindDirection.SOUTH;
        }

        if (windDirection.equals("SW"))
            return WindDirection.SOUTHWEST;
        if (windDirection.equals("SE"))
            return WindDirection.SOUTHEAST;

        if (windDirection.equals("E") || windDirection.equals("ENE")
                || windDirection.equals("ESE")) {
            return WindDirection.EAST;
        }

        throw new IllegalArgumentException("unknown wind direction: " + windDirection);
    }

    /**
     * Parses AccuWeather forecast HTML web page (supported format)
     * and returns its model representation.
     *
     * @param body AccuWeather forecast HTML web page (supported format)
     * @param timeOfDay requested forecast's time of day
     * @param forecastDate requested forecast's date
     * @param dateDifference a <b>supported</b> number of the days since current day
     *
     * @return a {@code WeatherForecast} instance containing values from
     * the requested forecast
     *
     * @throws RuntimeException if any component of the web page could not be found
     * or any value could not be parsed
     */
    private WeatherForecast parse(String body, TimeOfDay timeOfDay,
                                  LocalDate forecastDate, int dateDifference) {
        Pattern contentPattern = Pattern.compile("<div class=\"half-day-card-header\">[\\S\\s]+\\s*</div>\\s*<div class=\"half-day-card-content\">[\\S\\s]+</div>\\s*<div class=\"quarter-day-ctas\">");
        Pattern temperaturePattern = Pattern.compile("<div class=\"temperature\">\\s*(-*\\d+\\.*\\d*)");
        Pattern weatherSituationPattern = Pattern.compile("<div class=\"phrase\">(.+?)</div>");
        Pattern windPattern = Pattern.compile("Wind<span class=\"value\">([NWSE]+)\\s*(\\d+\\.*\\d*) km/h");
        Pattern humidityPattern = Pattern.compile("Humidity<span class=\"value\">(\\d+)%");

        Matcher contentMatcher = contentPattern.matcher(body);
        if (!contentMatcher.find())
            throw new RuntimeException("could not find content section");
        String contentBody = contentMatcher.group();

        float temperature;
        Matcher valueMatcher = temperaturePattern.matcher(contentBody);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find temperature value");
        try {
            temperature = Float.parseFloat(valueMatcher.group(1));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse temperature value", e);
        }

        WeatherSituation weatherSituation;
        valueMatcher = weatherSituationPattern.matcher(contentBody);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find weather conditions value");
        try {
            weatherSituation = parseWeatherSituation(valueMatcher.group(1));
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("could not parse weather conditions value", e);
        }

        float windSpeed;
        valueMatcher = windPattern.matcher(contentBody);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find wind speed value");
        try {
            windSpeed = Float.parseFloat(valueMatcher.group(2));
            //Convert from km/h to m/s
            windSpeed = windSpeed * 1000 / 3600;
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse wind speed value", e);
        }

        WindDirection windDirection;
        try {
            windDirection = parseWindDirection(valueMatcher.group(1));
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("could not parse wind direction value", e);
        }

        int humidity;
        valueMatcher = humidityPattern.matcher(contentBody);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find humidity value");
        try {
            humidity = Integer.parseInt(valueMatcher.group(1));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse humidity value", e);
        }

        return new WeatherForecast.Builder()
                .temperature(temperature)
                .humidity(humidity)
                .weatherSituation(weatherSituation)
                .windSpeed(windSpeed)
                .windDirection(windDirection)
                .timeOfDay(timeOfDay)
                .forecastDate(forecastDate)
                .dateDifference(dateDifference)
                .service(Service.ACCUWEATHER)
                .build();
    }

    /**
     * Gets {@link WeatherForecast} instance from AccuWeather.
     * This service provides 15 days forecast.
     *
     * @param forecastDate requested forecast date
     *
     * @return a {@link WeatherForecast} instance with AccuWeather forecast,
     * issued on the specified date and time, information.
     *
     * @throws IllegalArgumentException if forecast date is before or
     * 3 month later from current date
     * @throws RuntimeException if any error occurs when receiving or parsing
     * forecast data
     */
    @Override
    public WeatherForecast getWeatherForecast(TimeOfDay timeOfDay, LocalDate forecastDate) {
        int dateDifference = WeatherForecast.dateDifference(forecastDate, LocalDate.now());

        if (dateDifference < 0)
            throw new IllegalArgumentException("forecast date is before current date");
        if (dateDifference >= 15)
            throw new IllegalArgumentException("forecast date more than 15 days later");

        return parse(getHtmlBody(timeOfDay, dateDifference), timeOfDay, forecastDate,
                dateDifference);
    }
}
