package controller.receivers;

import model.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The {@code ForecaForecastReceiver} class secretly receives data from Foreca.
 *
 * @author Maxim Sergeev
 * @version 0.1.5
 */
public class ForecaForecastReceiver implements ForecastReceiver {
    /**
     * URL to Foreca
     */
    private final static String BASE_URL = "https://www.foreca.com/";

    /**
     * GET request string
     */
    private final static String REQUEST_STRING = "100463829/Zelenograd-Moscow-Russia/hourly?";

    /**
     * minus character used in Foreca that does not compatible with {@code parseFloat}.
     */
    private final static String DISASTER_MINUS = "−";

    /**
     * ASCII minus character compatible with {@code parseFloat}.
     */
    private final static String TRUE_MINUS = "-";

    /**
     * Constructs the {@code ForecaForecastReceiver}.
     */
    public ForecaForecastReceiver() {}

    /**
     * Returns Foreca forecast web page's text. Makes a sync GET request
     * to Foreca based on the parameters.
     *
     * @param dateDifference difference between current day of month and
     * the requested date's day of month
     *
     * @return Foreca forecast web page's text for the selected day
     *
     * @throws RuntimeException if failed to get response
     */
    private String getHtmlBody(int dateDifference) {
        String requestString = "day=" + dateDifference;

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(BASE_URL + REQUEST_STRING + requestString))
                .GET()
                .build();
        try {
            HttpResponse<String> response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != HttpURLConnection.HTTP_OK)
                throw new RuntimeException("bad request: " + request +  ". Status code: " + response.statusCode());
            return response.body();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("could not receive data from Foreca", e);
        }
    }

    /**
     * Returns model representation of Foreca weather situation description.
     *
     * @param weatherSituation Foreca weather situation description
     *
     * @return model representation of Foreca weather situation description
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private static WeatherSituation parseWeatherSituation(String weatherSituation)
            throws IllegalArgumentException {
        weatherSituation = weatherSituation.toLowerCase(Locale.ROOT);

        if (weatherSituation.contains("sunny")
                || weatherSituation.contains("clear"))
            return WeatherSituation.CLEAR;
        if (weatherSituation.contains("cloudy")
                || weatherSituation.contains("overcast"))
            return WeatherSituation.CLOUDY;
        if (weatherSituation.contains("rain")
                || weatherSituation.contains("showers")
                && !weatherSituation.contains("snow"))
            return WeatherSituation.RAIN;
        if (weatherSituation.contains("snow"))
            return WeatherSituation.SNOW;
        if (weatherSituation.contains("thunderstorm"))
            return WeatherSituation.THUNDERSTORM;
        throw new IllegalArgumentException("unknown weather situation: " + weatherSituation);
    }

    /**
     * Returns {@link WindDirection} value of the Foreca wind
     * direction representation.
     *
     * @param windDirection Foreca representation of wind direction
     *
     * @return a {@link WindDirection} value of the Foreca wind
     * direction representation
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private static WindDirection parseWindDirection(String windDirection)
            throws IllegalArgumentException {
        if (windDirection.equals("N"))
            return WindDirection.NORTH;
        if (windDirection.equals("W"))
            return WindDirection.WEST;
        if (windDirection.equals("S"))
            return WindDirection.SOUTH;
        if (windDirection.equals("E"))
            return WindDirection.EAST;
        if (windDirection.equals("NW"))
            return WindDirection.NORTHWEST;
        if (windDirection.equals("NE"))
            return WindDirection.NORTHEAST;
        if (windDirection.equals("SW"))
            return WindDirection.SOUTHWEST;
        if (windDirection.equals("SE"))
            return WindDirection.SOUTHEAST;
        throw new IllegalArgumentException("unknown wind direction: " + windDirection);
    }

    /**
     * Parses Foreca forecast HTML web page (supported format)
     * and returns its model representation.
     *
     * @param body Foreca forecast HTML web page (supported format)
     * @param timeOfDay requested forecast's time of day
     * @param forecastDate requested forecast's date
     * @param dateDifference a <b>supported</b> number of the days since current day
     *
     * @return a {@code WeatherForecast} instance containing values from
     * the requested forecast
     *
     * @throws RuntimeException if any component of the web page could not be found
     * or any value could not be parsed
     * @throws IllegalArgumentException if {@code dayDifference} is greater
     * than the supported number of days
     */
    private static WeatherForecast parse(String body, TimeOfDay timeOfDay,
                                         LocalDate forecastDate, int dateDifference) {
        /*
        * The Foreca "hourly" forecast webpage consists of a table in which
        * each forecast corresponds to a specific hour in the day of the
        * selected date. Processing one row of the table is enough to ensure
        * the uniqueness of the names of the value containers.
        */
        Pattern hourlyForecastContainerPattern = Pattern.compile("<div class=\"hourContainer\">(.+?)<div class=\"explainer\">");
        Pattern forecastRowPattern = Pattern.compile("<div class=\".+?\"><div class=\"row\"><div class=\"time\"><span class=\"value time time_12h\">.+?</span> <span class=\"value time time_24h\">.+?</span></div>(.+?)<span class=\"precipChance\">.+?</span>");
        Pattern weatherSituationPattern = Pattern.compile("<div class=\"textRow\"><div class=\"time\"></div><div class=\"symbolText\">(.+?)</div>");
        Pattern temperaturePattern = Pattern.compile("<span class=\"t\"><span class=\"value temp temp_f\">.+?</span> <span class=\"value temp temp_c .+?\">(.+?)</span> </span>");
        Pattern humidityPattern = Pattern.compile("<span class=\"humidity\">(.+?)<span class=\"unit\">");
        Pattern windSpeedPattern = Pattern.compile("<div class=\"wind\">.+?<span class=\"value wind wind_ms\">(.+?)</span></span></div>");
        Pattern windDirectionPattern = Pattern.compile("<div class=\"wind\"><img src=.+? alt=\"(.+?)\" width=\"30\" height=\"30\">");

        //Indexes of the rows in a table
        final int MIDNIGHT_INDEX = 0;
        final int MORNING_INDEX = 6;
        final int AFTERNOON_INDEX = 12;
        final int EVENING_INDEX = 18;

        //Requested time of the day
        int timeOfDayIndex = switch (timeOfDay) {
            case MIDNIGHT -> MIDNIGHT_INDEX;
            case MORNING -> MORNING_INDEX;
            case AFTERNOON -> AFTERNOON_INDEX;
            case EVENING -> EVENING_INDEX;
        };

        if (forecastDate.equals(LocalDate.now())) {
            if (LocalTime.now().getHour() > timeOfDayIndex)
                throw new IllegalArgumentException("forecast time is before current time");

            timeOfDayIndex = timeOfDayIndex - LocalTime.now().getHour();
        }

        Matcher hourlyForecastContainerMatcher = hourlyForecastContainerPattern.matcher(body);
        if(!hourlyForecastContainerMatcher.find())
            throw new RuntimeException("could not find forecasts table");

        String hourlyForecastContainerBody = hourlyForecastContainerMatcher.group(1);
        Matcher forecastRowMatcher = forecastRowPattern.matcher(hourlyForecastContainerBody);
        for (int i = 0; i <= timeOfDayIndex; ++i)
            if (!forecastRowMatcher.find())
                throw new RuntimeException("could not find forecast for specified time of the day");
        String forecastRow = forecastRowMatcher.group(1);

        float temperature;
        Matcher valueMatcher = temperaturePattern.matcher(forecastRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find temperature value");
        try {
            temperature = Float.parseFloat(valueMatcher.group(1).replace(DISASTER_MINUS, TRUE_MINUS));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse temperature value", e);
        }

        WeatherSituation weatherSituation;
        valueMatcher = weatherSituationPattern.matcher(forecastRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find weather situation value");
        try {
            weatherSituation = parseWeatherSituation(valueMatcher.group(1));
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("could not parse weather situation value", e);
        }

        int humidity;
        valueMatcher = humidityPattern.matcher(forecastRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find humidity value");
        try {
            humidity = Integer.parseInt(valueMatcher.group(1));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse humidity value", e);
        }

        int windSpeed;
        valueMatcher = windSpeedPattern.matcher(forecastRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find wind speed value");
        try {
            windSpeed = Integer.parseInt(valueMatcher.group(1));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse wind speed value", e);
        }

        WindDirection windDirection;
        valueMatcher = windDirectionPattern.matcher(forecastRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find wind direction value");
        try {
            windDirection = parseWindDirection(valueMatcher.group(1));
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("could not parse wind direction value", e);
        }

        return new WeatherForecast.Builder()
                .temperature(temperature)
                .humidity(humidity)
                .weatherSituation(weatherSituation)
                .windSpeed(windSpeed)
                .windDirection(windDirection)
                .forecastDate(forecastDate)
                .timeOfDay(timeOfDay)
                .dateDifference(dateDifference)
                .service(Service.FORECA)
                .build();
    }

    /**
     * Gets {@link WeatherForecast} instance from Foreca (secretly).
     * This service provides 10-day forecast.
     *
     * @param timeOfDay requested forecast's time of day
     * @param forecastDate requested forecast's date
     *
     * @return a {@link WeatherForecast} instance with Foreca forecast,
     * issued on the specified date and time, information.
     *
     * @throws IllegalArgumentException if forecast date is before or
     * more than 10 days later from current date
     * @throws RuntimeException if any error occurs when receiving or parsing
     * forecast data
     */
    @Override
    public WeatherForecast getWeatherForecast(TimeOfDay timeOfDay, LocalDate forecastDate) {
        int dateDifference = WeatherForecast.dateDifference(forecastDate, LocalDate.now());

        if (dateDifference < 0)
            throw new IllegalArgumentException("forecast date is before current date");
        if (dateDifference >= 10)
            throw new IllegalArgumentException("forecast date more than 10 days later");

        return parse(getHtmlBody(dateDifference), timeOfDay, forecastDate, dateDifference);
    }
}
