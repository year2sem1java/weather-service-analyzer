package controller.receivers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import java.time.LocalDate;
import java.util.regex.*;

import model.*;

/**
 * The {@code YandexWeatherForecastReceiver} class secretly receives data from Yandex.Weather.
 *
 * @author Georgij Krajnyukov
 * @version 0.3.3
 */
public class YandexWeatherForecastReceiver implements ForecastReceiver {
    /**
     * URL to Yandex.Weather
     */
    private final static String BASE_URL = "https://yandex.ru/pogoda/details?";

    /**
     * GET request string
     */
    private final static String REQUEST_STRING = "lat=55.991893&lon=37.214382&via=ms";

    /**
     * minus character used in Yandex.Weather that does not compatible with {@code parseFloat}.
     */
    private final static String DISASTER_MINUS = "−";

    /**
     * ASCII minus character compatible with {@code parseFloat}.
     */
    private final static String TRUE_MINUS = "-";

    /**
     * Constructs the {@code YandexWeatherForecastReceiver}.
     */
    public YandexWeatherForecastReceiver() {}

    /**
     * Returns Yandex.Weather forecast web page's text.
     * Makes a sync GET request to Yandex.Weather (Savyolki, Zelenograd).
     *
     * @return Yandex.Weather 10-day forecast web page's text
     *
     * @throws RuntimeException if failed to get response
     */
    private String getHtmlBody() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest requestString = HttpRequest.newBuilder()
                .uri(URI.create(BASE_URL + REQUEST_STRING))
                .GET()
                .build();
        try {
            HttpResponse<String> response = client.send(requestString,
                    HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != HttpURLConnection.HTTP_OK)
                throw new RuntimeException("bad request: " + requestString +  ". Status code: " + response.statusCode());
            return response.body();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("could not receive data from Yandex.Weather", e);
        }
    }

    /**
     * Returns model representation of Yandex.Weather weather situation description.
     *
     * @param weatherSituation Yandex.Weather weather situation description
     *
     * @return model representation of Yandex.Weather weather situation description
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private static WeatherSituation parseWeatherSituation(String weatherSituation)
            throws IllegalArgumentException {
        if (weatherSituation.equals("Ясно"))
            return WeatherSituation.CLEAR;
        if (weatherSituation.equals("Малооблачно")
                || weatherSituation.equals("Облачно с прояснениями")
                || weatherSituation.equals("Пасмурно"))
            return WeatherSituation.CLOUDY;
        if (weatherSituation.equals("Небольшой дождь")
                || weatherSituation.equals("Дождь")
                || weatherSituation.equals("Умеренно сильный дождь")
                || weatherSituation.equals("Сильный дождь")
                || weatherSituation.equals("Длительный сильный дождь")
                || weatherSituation.equals("Дождь с грозой")
                || weatherSituation.equals("Ливень")
                || weatherSituation.equals("Морось"))
            return WeatherSituation.RAIN;
        if (weatherSituation.equals("Дождь со снегом")
                || weatherSituation.equals("Небольшой снег")
                || weatherSituation.equals("Снег")
                || weatherSituation.equals("Снегопад")
                || weatherSituation.equals("Град")
                || weatherSituation.equals("Гроза с градом"))
            return WeatherSituation.SNOW;
        if (weatherSituation.equals("Гроза"))
            return WeatherSituation.THUNDERSTORM;
        throw new IllegalArgumentException("unknown weather situation: " + weatherSituation);
    }

    /**
     * Returns {@link WindDirection} value of the Yandex.Weather wind
     * direction representation.
     *
     * @param windDirection Yandex.Weather representation of wind direction
     *
     * @return a {@link WindDirection} value of the Yandex.Weather wind
     * direction representation
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private static WindDirection parseWindDirection(String windDirection)
            throws IllegalArgumentException {
        if (windDirection.equals("С"))
            return WindDirection.NORTH;
        if (windDirection.equals("З"))
            return WindDirection.WEST;
        if (windDirection.equals("Ю"))
            return WindDirection.SOUTH;
        if (windDirection.equals("В"))
            return WindDirection.EAST;
        if (windDirection.equals("СЗ"))
            return WindDirection.NORTHWEST;
        if (windDirection.equals("СВ"))
            return WindDirection.NORTHEAST;
        if (windDirection.equals("ЮЗ"))
            return WindDirection.SOUTHWEST;
        if (windDirection.equals("ЮВ"))
            return WindDirection.SOUTHEAST;
        throw new IllegalArgumentException("unknown wind direction: " + windDirection);
    }

    /**
     * Parses Yandex.Weather forecast HTML web page (supported format)
     * and returns its model representation.
     *
     * @param body Yandex.Weather forecast HTML web page (supported format)
     * @param timeOfDay requested forecast's time of day
     * @param forecastDate requested forecast's date
     * @param dateDifference a <b>supported</b> number of the days since current day
     *
     * @return a {@code WeatherForecast} instance containing values from
     * the requested forecast
     *
     * @throws RuntimeException if any component of the web page could not be found
     * or any value could not be parsed
     * @throws IllegalArgumentException if {@code dayDifference} is greater
     * than the supported number of days
     */
    private static WeatherForecast parse(String body, TimeOfDay timeOfDay,
                                         LocalDate forecastDate, int dateDifference) {
        if (dateDifference >= 10)
            throw new IllegalArgumentException("Yandex.Weather supports only 10-days forecast");
        /*
        * Yandex.Weather "10 day detailed forecast" web page
        * consists of cards corresponding to a particular date with 4 rows
        * corresponding to the one of times of the day. Processing a single
        * row is enough to provide values container names uniqueness.
        */
        Pattern cardPattern = Pattern.compile("<div class=\"card\"><dt.*?>.+?</dt><dd.*?>(.+?)</dd></div>");
        Pattern cardRowPattern = Pattern.compile("<tr class=\"weather-table__row\">(.+?)</tr>");
        Pattern temperaturePattern = Pattern.compile("<span class=\"temp__value temp__value_with-unit\">([\\x{2212}+]\\d+\\.*\\d*)</span>");
        Pattern weatherSituationPattern = Pattern.compile("<td class=\"weather-table__body-cell weather-table__body-cell_type_condition\">([А-Яа-яё\\s]+)</td>");
        Pattern humidityPattern = Pattern.compile("<td class=\"weather-table__body-cell weather-table__body-cell_type_humidity\">(\\d+)%</td>");
        Pattern windSpeedPattern = Pattern.compile("<span class=\"wind-speed\">(\\d+),(\\d+)|(Штиль)</span>");
        Pattern windDirectionPattern = Pattern.compile("<abbr class=\" icon-abbr\" title=\"Ветер: [а-я-]+\" aria-label=\"Ветер: [а-я-]+\" role=\"text\">([А-Я]+)</abbr>");

        //Indexes of the rows in a card
        final int MORNING_INDEX = 0;
        final int AFTERNOON_INDEX = 1;
        final int EVENING_INDEX = 2;
        final int MIDNIGHT_INDEX = 3;

        //Requested time of the day
        int timeOfDayIndex = switch (timeOfDay) {
            case MIDNIGHT -> MIDNIGHT_INDEX;
            case MORNING -> MORNING_INDEX;
            case AFTERNOON -> AFTERNOON_INDEX;
            case EVENING -> EVENING_INDEX;
        };

        Matcher cardMatcher = cardPattern.matcher(body);
        for (int i = 0; i <= dateDifference; ++i)
            if (!cardMatcher.find())
                throw new RuntimeException("could not find forecast for specified day");

        String cardBody = cardMatcher.group(1);
        Matcher cardRowMatcher = cardRowPattern.matcher(cardBody);
        for (int i = 0; i <= timeOfDayIndex; ++i)
            if (!cardRowMatcher.find())
                throw new RuntimeException("could not find forecast for specified time of the day");
        String cardRow = cardRowMatcher.group(1);

        float temperature;
        Matcher valueMatcher = temperaturePattern.matcher(cardRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find temperature value");
        try {
            temperature = Float.parseFloat(valueMatcher.group(1).replace(DISASTER_MINUS, TRUE_MINUS));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse temperature value", e);
        }

        WeatherSituation weatherSituation;
        valueMatcher = weatherSituationPattern.matcher(cardRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find weather situation value");
        try {
            weatherSituation = parseWeatherSituation(valueMatcher.group(1));
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("could not parse weather situation value", e);
        }

        int humidity;
        valueMatcher = humidityPattern.matcher(cardRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find humidity value");
        try {
            humidity = Integer.parseInt(valueMatcher.group(1));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse humidity value", e);
        }

        float windSpeed;
        valueMatcher = windSpeedPattern.matcher(cardRow);
        if (!valueMatcher.find())
            throw new RuntimeException("could not find wind speed value");
        try {
            if (valueMatcher.group(3) != null)
                windSpeed = 0.0f;
            else
                windSpeed = Float.parseFloat(valueMatcher.group(1) + "." + valueMatcher.group(2));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse wind speed value", e);
        }

        WindDirection windDirection;
        valueMatcher = windDirectionPattern.matcher(cardRow);
        if (!valueMatcher.find()) {
            if (windSpeed == 0.0f)
                windDirection = WindDirection.NONE;
            else
                throw new RuntimeException("could not find wind direction value");
        } else {
            try {
                windDirection = parseWindDirection(valueMatcher.group(1));
            } catch (IllegalArgumentException e) {
                throw new RuntimeException("could not parse wind direction value", e);
            }
        }

        return new WeatherForecast.Builder()
                .temperature(temperature)
                .humidity(humidity)
                .weatherSituation(weatherSituation)
                .windSpeed(windSpeed)
                .windDirection(windDirection)
                .forecastDate(forecastDate)
                .timeOfDay(timeOfDay)
                .dateDifference(dateDifference)
                .service(Service.YANDEX_WEATHER)
                .build();
    }

    /**
     * Gets {@link WeatherForecast} instance from Yandex.Weather (secretly).
     * This service provides 10-day forecast.
     *
     * @param timeOfDay requested forecast's time of day
     * @param forecastDate requested forecast's date
     *
     * @return a {@link WeatherForecast} instance with Yandex.Weather forecast,
     * issued on the specified date and time, information.
     *
     * @throws IllegalArgumentException if forecast date is before or
     * more than 10 days later from current date
     * @throws RuntimeException if any error occurs when receiving or parsing
     * forecast data
     */
    @Override
    public WeatherForecast getWeatherForecast(TimeOfDay timeOfDay, LocalDate forecastDate) {
        int dateDifference = WeatherForecast.dateDifference(forecastDate, LocalDate.now());

        if (dateDifference < 0)
            throw new IllegalArgumentException("forecast date is before current date");
        if (dateDifference >= 10)
            throw new IllegalArgumentException("forecast date more than 10 days later");

        return parse(getHtmlBody(), timeOfDay, forecastDate, dateDifference);
    }
}
