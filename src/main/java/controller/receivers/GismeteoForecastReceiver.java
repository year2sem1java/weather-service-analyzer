package controller.receivers;

import model.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The {@code GismeteoForecastReceiver} class receives data from Gismeteo.
 *
 * @author Georgij Krajnyukov
 * @version 0.3.6
 */
public class GismeteoForecastReceiver implements ForecastReceiver {
    /**
     * URL to Gismeteo (Zelenograd)
     */
    private static final String BASE_URL = "https://www.gismeteo.com/weather-zelenograd-11443";

    /**
     * GET request's HTTP header User-Agent value
     */
    private static final String USER_AGENT_STRING = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 OPR/80.0.4170.40";

    /**
     * ASCII minus character compatible with {@code parseFloat}.
     */
    private static final String DISASTER_MINUS = "&minus;";

    /**
     * ASCII minus character compatible with {@code parseFloat}.
     */
    private final static String TRUE_MINUS = "-";

    /**
     * Constructs the {@code GismeteoForecastReceiver}.
     */
    public GismeteoForecastReceiver() {}

    /**
     * Returns Gismeteo forecast web page's text. Makes a sync GET request
     * to Gismeteo based on the parameters.
     *
     * @param dateDifference difference between current day of month and
     * the requested date's day of month
     *
     * @return Gismeteo forecast web page's text
     *
     * @throws RuntimeException if failed to get response
     */
    private String getHtmlBody(int dateDifference) {
        String requestString = switch (dateDifference) {
            case 0 -> "/";
            case 1 -> "/tomorrow/";
            default -> "/" + (dateDifference + 1) + "-day/";
        };

        HttpClient client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(BASE_URL + requestString))
                .header("User-Agent", USER_AGENT_STRING)
                .GET()
                .build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != HttpURLConnection.HTTP_OK)
                throw new RuntimeException("bad request: " + requestString +  ". Status code: " + response.statusCode());
            return response.body();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("could not receive data from Gismeteo", e);
        }
    }

    /**
     * Returns model representation of Gismeteo weather situation description.
     *
     * @param weatherSituation Gismeteo weather situation description
     *
     * @return model representation of Gismeteo weather situation description
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private WeatherSituation parseWeatherSituation(String weatherSituation) {
        weatherSituation = weatherSituation.toLowerCase(Locale.ROOT);

        if (weatherSituation.contains("brume"))
            return WeatherSituation.FOG;
        if (weatherSituation.contains("rain"))
            return WeatherSituation.RAIN;
        if (weatherSituation.contains("snow"))
            return WeatherSituation.SNOW;
        if (weatherSituation.contains("cloudy"))
            return WeatherSituation.CLOUDY;
        if (weatherSituation.contains("fair"))
            return WeatherSituation.CLEAR;
        throw new IllegalArgumentException("unknown weather situation: " + weatherSituation);
    }

    /**
     * Returns model representation of Gismeteo wind direction representation.
     *
     * @param windDirection Gismeteo representation of wind direction
     *
     * @return a {@link WindDirection} value of the Gismeteo wind
     * direction representation
     *
     * @throws IllegalArgumentException if any unknown value is passed
     */
    private WindDirection parseWindDirection(String windDirection) {
        if (windDirection.equals("N"))
            return WindDirection.NORTH;
        if (windDirection.equals("W"))
            return WindDirection.WEST;
        if (windDirection.equals("S"))
            return WindDirection.SOUTH;
        if (windDirection.equals("E"))
            return WindDirection.EAST;
        if (windDirection.equals("NW"))
            return WindDirection.NORTHWEST;
        if (windDirection.equals("NE"))
            return WindDirection.NORTHEAST;
        if (windDirection.equals("SW"))
            return WindDirection.SOUTHWEST;
        if (windDirection.equals("SE"))
            return WindDirection.SOUTHEAST;
        throw new IllegalArgumentException("unknown wind direction: " + windDirection);
    }

    /**
     * Parses Gismeteo forecast HTML web page (supported format)
     * and returns its model representation.
     *
     * @param body Gismeteo forecast HTML web page (supported format)
     * @param timeOfDay requested forecast's time of day
     * @param forecastDate requested forecast's date
     * @param dateDifference a <b>supported</b> number of the days since current day
     *
     * @return a {@code WeatherForecast} instance containing values from
     * the requested forecast
     *
     * @throws RuntimeException if any component of the web page could not be found
     * or any value could not be parsed
     */
    private WeatherForecast parse(String body, TimeOfDay timeOfDay,
                                  LocalDate forecastDate, int dateDifference) {
        final int MIDNIGHT_INDEX = 0;
        final int MORNING_INDEX = 2;
        final int AFTERNOON_INDEX = 4;
        final int EVENING_INDEX = 6;

        int valueIndex = switch (timeOfDay) {
            case MIDNIGHT -> MIDNIGHT_INDEX;
            case MORNING -> MORNING_INDEX;
            case AFTERNOON -> AFTERNOON_INDEX;
            case EVENING -> EVENING_INDEX;
        };

        Pattern weatherSection = Pattern.compile("<section class=\"section section-content\" data-column-place=\"C1P3\"><div class=\"widget widget-weather widget-oneday\" data-widget=\"weather\" data-stat-type=\"widget\" data-stat-value=\"weather\">.+<div class=\"widget-row widget-row-icon\">(.+?)</div><div class=\"widget-row-chart widget-row-chart-temperature\">(.+?)</div><div class=\"widget-row widget-row-wind-speed-gust row-with-caption\">.+?</div></section>");
        Pattern weatherSituationValue = Pattern.compile("<div class=\"weather-icon tooltip\" data-text=\"(.+?)\">");
        Pattern temperatureValue = Pattern.compile("<span class=\"unit unit_temperature_c\">(.+?)</span>");

        Pattern windSection = Pattern.compile("<div class=\"widget-row widget-row-wind-speed\">(.+)?<div class=\"widget-row widget-row-wind-direction\">(.+)?<div class=\"widget-row widget-row-wind-gust row-with-caption\">");
        Pattern windSpeedValue = Pattern.compile("<span class=\"wind-unit unit unit_wind_m_s\">\\s*(\\d+)\\s*</span>");
        Pattern windDirectionValue = Pattern.compile("<div class=\"direction\">(.+?)</div>|<use xlink:href=\"#wind-zero\"/>");

        Pattern humiditySection = Pattern.compile("<section class=\"section section-content\" data-column-place=\"C1P11\"><div class=\"widget widget-humidity widget-oneday\" data-widget=\"humidity\" data-stat-type=\"widget\" data-stat-value=\"humidity\">.+?<div class=\"widget-body widget-columns-8\"><div class=\"widget-wrap\">(.+?)</div></section>");
        Pattern humidityValue = Pattern.compile("<div class=\"row-item item-.+?\">(.+?)</div>");

        float temperature;
        int humidity;
        WeatherSituation weatherSituation;
        int windSpeed;
        WindDirection windDirection;

        Matcher weatherSectionMatcher = weatherSection.matcher(body);
        if (!weatherSectionMatcher.find())
            throw new RuntimeException("could not find weather section");

        Matcher windSectionMatcher = windSection.matcher(body);
        if (!windSectionMatcher.find())
            throw new RuntimeException("could not find wind section");

        Matcher humiditySectionMatcher = humiditySection.matcher(body);
        if (!humiditySectionMatcher.find())
            throw new RuntimeException("could not find humidity section");

        String weatherSituationRow = weatherSectionMatcher.group(1);
        String temperatureRow = weatherSectionMatcher.group(2);
        String windSpeedRow = windSectionMatcher.group(1);
        String windDirectionRow = windSectionMatcher.group(2);
        String humidityRow = humiditySectionMatcher.group(1);

        Matcher weatherSituationMatcher = weatherSituationValue.matcher(weatherSituationRow);
        Matcher temperatureMatcher = temperatureValue.matcher(temperatureRow);
        Matcher windSpeedMatcher = windSpeedValue.matcher(windSpeedRow);
        Matcher windDirectionMatcher = windDirectionValue.matcher(windDirectionRow);
        Matcher humidityMatcher = humidityValue.matcher(humidityRow);

        for (int i = 0; i <= valueIndex; ++i) {
            if (!weatherSituationMatcher.find())
                throw new RuntimeException("could not find requested weather situation");
            if (!temperatureMatcher.find())
                throw new RuntimeException("could not find requested temperature");
            if (!windSpeedMatcher.find())
                throw new RuntimeException("could not find requested wind speed");
            if (!windDirectionMatcher.find())
                throw new RuntimeException("could not find requested wind direction");
            if (!humidityMatcher.find())
                throw new RuntimeException("could not find requested humidity");
        }

        weatherSituation = parseWeatherSituation(weatherSituationMatcher.group(1));

        try {
            temperature = Float.parseFloat(temperatureMatcher.group(1)
                    .replace(DISASTER_MINUS, TRUE_MINUS));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse temperature value", e);
        }

        if (windDirectionMatcher.group(1) == null)
            windDirection = WindDirection.NONE;
        else
            windDirection = parseWindDirection(windDirectionMatcher.group(1));

        try {
            windSpeed = Integer.parseInt(windSpeedMatcher.group(1));
        } catch (NumberFormatException e) {
            throw new RuntimeException("could not parse wind speed value", e);
        }

        humidity = Integer.parseInt(humidityMatcher.group(1));

        return (new WeatherForecast.Builder())
                .temperature(temperature)
                .humidity(humidity)
                .weatherSituation(weatherSituation)
                .windSpeed(windSpeed)
                .windDirection(windDirection)
                .forecastDate(forecastDate)
                .timeOfDay(timeOfDay)
                .dateDifference(dateDifference)
                .service(Service.GISMETEO)
                .build();
    }

    /**
     * Gets {@link WeatherForecast} instance from Gismeteo.
     * This service provides 3-month forecast.
     *
     * @param forecastDate requested forecast date
     *
     * @return a {@link WeatherForecast} instance with Gismeteo forecast,
     * issued on the specified date and time, information.
     *
     * @throws IllegalArgumentException if forecast date is before or
     * 10-days away from current date
     * @throws RuntimeException if any error occurs when receiving or parsing
     * forecast data
     */
    @Override
    public WeatherForecast getWeatherForecast(TimeOfDay timeOfDay, LocalDate forecastDate) {
        int dateDifference = WeatherForecast.dateDifference(forecastDate, LocalDate.now());

        if (dateDifference < 0)
            throw new IllegalArgumentException("forecast date is before current date");
        if (dateDifference >= 10)
            throw new IllegalArgumentException("forecast date more than 10 days later");

        return parse(getHtmlBody(dateDifference), timeOfDay, forecastDate, dateDifference);
    }
}
