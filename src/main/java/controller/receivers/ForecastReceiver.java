package controller.receivers;

import model.TimeOfDay;
import model.WeatherForecast;

import java.time.LocalDate;

/**
 * The {@code ForecastReceiver} receives forecast data from a particular source
 * and then returns its representation.
 *
 * @author Georgij Krajnyukov
 * @version 0.3.0
 */
public interface ForecastReceiver {
    /**
     * Gets {@link WeatherForecast} instance with the same forecast date
     * and time information.
     *
     * @param timeOfDay time of the day for which forecast was made
     * @param forecastDate date for which forecast was made
     *
     * @return a {@link WeatherForecast} instance with the forecast
     * issued on the specified date and time.
     */
    WeatherForecast getWeatherForecast(TimeOfDay timeOfDay, LocalDate forecastDate);
}
